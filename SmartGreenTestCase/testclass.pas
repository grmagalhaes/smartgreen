unit TestClass;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls;

type

  TTestCase = class
    public
      constructor Create (S : TEdit; E: TEdit; M : TMemo);
      procedure Clear;
      procedure Igual(C: String; Expected: String);
      procedure Diferente(C: String; Expected: String);
    private
      CountOK    : Longint;
      CountError : Longint;
      SucessoEdit : TEdit;
      ErroEdit   : TEdit;
      Memo : TMemo;
  end;

implementation

constructor TTestCase.Create(S : TEdit; E: TEdit; M : TMemo);
begin
  CountOK    := 0;
  CountError := 0;
  SucessoEdit := S;
  ErroEdit := E;
  Memo := M;
  SucessoEdit.Text := InttoStr(CountOK);
  ErroEdit.Text := InttoStr(CountError);
  Memo.Lines.Clear;

end;

procedure TTestCase.Clear;
begin
  CountOK    := 0;
  CountError := 0;
  SucessoEdit.Text := InttoStr(CountOK);
  ErroEdit.Text := InttoStr(CountError);
  Memo.Lines.Clear;
end;

procedure TTestCase.Igual(C: String; Expected: String);
var Resultado : Boolean;
    ResultadoStr : String;
begin
  Resultado := C = Expected;
  if (Resultado) then
  begin
    ResultadoStr := 'OK';
    Inc(CountOK);
  end
  else
  begin
    ResultadoStr := 'ERRO';
    Inc(CountError);
  end;

  SucessoEdit.Text := InttoStr(CountOK);
  ErroEdit.Text := InttoStr(CountError);
  Memo.Append('Recebido: '+ C + ', Esperado: ' + Expected + ', Resultado: ' + ResultadoStr);
end;

procedure TTestCase.Diferente(C: String; Expected: String);
var Resultado : Boolean;
    ResultadoStr : String;
begin
  Resultado := C <> Expected;
  if (Resultado) then
  begin
    ResultadoStr := 'OK';
    Inc(CountOK);
  end
  else
  begin
    ResultadoStr := 'ERRO';
    Inc(CountError);
  end;

  SucessoEdit.Text := InttoStr(CountOK);
  ErroEdit.Text := InttoStr(CountError);
  Memo.Append('Recebido: '+ C + ', Esperado: ' + Expected + ', Resultado: ' + ResultadoStr);
end;

end.

