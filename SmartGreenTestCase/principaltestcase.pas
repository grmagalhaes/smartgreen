unit PrincipalTestCase;

// TestCase criado por Gerson Rodrigues

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  ExtCtrls, StdCtrls, TestClass, Card;

type

  { TTestForm }

  TTestForm = class(TForm)
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    Teste1: TBitBtn;
    SucessoEdt: TEdit;
    ErroEdt: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    MemoTest: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Teste1Click(Sender: TObject);
  private

  end;

var
  TestForm: TTestForm;
  T: TTestCase;


implementation

{$R *.lfm}

{ TTestForm }

procedure TTestForm.FormCreate(Sender: TObject);
begin
  T := TTestCase.Create(SucessoEdt, ErroEdt, MemoTest);
end;

procedure TTestForm.BitBtn1Click(Sender: TObject);
var C : TCard;
begin
  T.Clear;
  C := TCard.Create(Self);
  C.SetPassword('123456');
  T.Igual(C.GetPassword(), '8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92');
  C.Free;
end;

procedure TTestForm.Teste1Click(Sender: TObject);
var C: TCard;
begin
  T.Clear;
  C := TCard.Create(Self);

  C.SetNumber('5453010000066167');   T.Igual(C.GetCardBrand, 'MASTERCARDx');
  C.SetNumber('5555666677778884');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480450322436382');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480456629742240');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480454162817040');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480458529684902');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480452215505109');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480457572486140');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480459239819424');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480450643291698');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480453894074326');   T.Igual(C.GetCardBrand, 'MASTERCARD');
  C.SetNumber('5480453518154389');   T.Igual(C.GetCardBrand, 'MASTERCARD');

  C.SetNumber('4012001037141112');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4012001038443335');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4485088329228200');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4716029876210861');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4532380508811952');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4532803880412495');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4466675250169297');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4556022261221794');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4716145657249124');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4539242404735532');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4916244175522001');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4716232616269243');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4235647728025682');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4073020000000002');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4012001037141112');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4551870000000183');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4343994826655179');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4720915509578076');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4061379008400646');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4076774689941665');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4369665585758359');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4379764175816814');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4332149024548964');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4384313432156102');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4361231929117410');   T.Igual(C.GetCardBrand, 'VISA');
  C.SetNumber('4375133936468386');   T.Igual(C.GetCardBrand, 'VISA');

  C.SetNumber('376449047333005');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('376411112222331');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('374244037331636');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('379936207638865');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('375674696629418');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('378719889685045');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('376617469450552');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('372767748763368');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('373619171521939');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('375663449749698');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('374530640203661');   T.Igual(C.GetCardBrand, 'AMEX');
  C.SetNumber('374013357823153');   T.Igual(C.GetCardBrand, 'AMEX');

  C.SetNumber('6362970000457013');  T.Igual(C.GetCardBrand, 'ELO');

  C.SetNumber('5078601912345600019');   T.Igual(C.GetCardBrand, 'AURA');
  C.SetNumber('5078601870000127985');   T.Igual(C.GetCardBrand, 'AURA');

  C.SetNumber('3841001111222233334');   T.Igual(C.GetCardBrand, 'HIPERCARD');

  C.Free;

end;

end.

