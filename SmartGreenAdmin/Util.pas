unit Util;

// classe criada por Gerson Rodrigues - 31/08

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,lazutf8, lclintf, fphttpclient;

type

{Minhas classes utilitárias *}

TSha256Array = array[0..31] of byte;

TUTil = class
  public
    class function CurrentExeDir() : String; static;
    class function StringToHex(S: String): String; static;
    class function HexToString(S: string): string; static;
    class function BinToHex(S: TSha256Array): string;
    class procedure AbrirPagina(URL : String);
    class function EncodeURLElement(S: String): String;
    class function HTTPSend(Service: String; HTTPRequest : String; var HTTPStatus : String; var HTTPResponse : String) : Boolean;
    class function isCNPJ(CNPJ: string): boolean;
end;

implementation

class function TUtil.CurrentExeDir() : String;
begin
  {$IFDEF DARWIN}
  Result := ExtractFilePath(ParamStr(0)) + '../../../';
  {$ENDIF}
  {$IFDEF WINDOWS}
  Result := ExtractFilePath(ParamStr(0))
  {$ENDIF}
end;

class function TUtil.StringToHex(S: string): string;
var
  i: integer;

begin
  Result := '';

  for i := 1 to Length( S ) do
    Result := Result + IntToHex( Ord( S[i] ), 2 );
end;

class function TUtil.HexToString(S: string): string;
var
  i: integer;

begin
  Result := '';

  for i := 1 to Length( S ) do
  begin
    if ((i mod 2) = 1) then
      Result := Result + Chr( StrToInt( '0x' + Copy( S, i, 2 )));
  end;
end;

class function TUtil.BinToHex(S: TSha256Array): string;
var
  i: integer;

begin
  Result := '';

  for i := 0 to Length(S) - 1 do
    Result := Result + IntToHex( Ord( S[i] ), 2 );
end;

class procedure TUtil.AbrirPagina(URL : String);
begin
  if (Pos(' ', URL) <> 0) then URL := Copy(URL, 1, Pos(' ', URL) - 1);
  if (URL <> '') then
  begin
    if (Copy(URL, 1, 3) = 'www') then
      URL := 'http://' + URL;

    if (Copy(URL, 1, 4) = 'http') then
      OpenURL(EncodeURLElement(URL)) { *Converted from ShellExecute* }
    else
      OpenDocument(SystoUTF8(URL));
  end;
end;

class function TUtil.EncodeURLElement(S: String): String;
//const  NotAllowed = [ ';', '/', '?', ':', '@', '=', '&', '#', '+', '_', '<', '>',
//                      '"', '%', '{', '}', '|', '\', '^', '~', '[', ']', '`' ];

var i, O, l : Integer;
      h: string[2];
      P : PChar;
      c: AnsiChar;

begin
  l := Length(S);
  if (l=0) then Exit;
  SetLength(Result,l * 3);
  P := Pchar(Result);
  for I := 1 to L do
  begin
    C := S[i];
    O := Ord(c);
//    if (O<=$20) or (O>=$7F) or (c in NotAllowed) then
    if (O>=$7F) then
    begin
      P^ := '%';
      Inc(P);
      h := IntToHex(Ord(c), 2);
      p^ := h[1];
      Inc(P);
      p^ := h[2];
      Inc(P);
    end
    else
    begin
      P^ := c;
      Inc(p);
    end;
  end;
  SetLength(Result,P-PChar(Result));
end;

// envia a requisição para o REST Server. O status e a resposta retornadas são retornados para gravar a transação
class function TUtil.HTTPSend(Service: String; HTTPRequest : String; var HTTPStatus : String; var HTTPResponse : String) : Boolean;
var
  httpClient : TFPHTTPClient;
begin
  HTTPResponse := '';
  HTTPStatus := '';
  httpClient := TFPHTTPClient.Create(nil);
  httpClient.AddHeader('Accept','application/json');
  httpClient.AddHeader('Content-Type','application/json');

  httpClient.RequestBody := TStringStream.Create(HTTPRequest);
  try
    HTTPResponse := httpClient.Post(Service);
    HTTPStatus := httpClient.ResponseStatusText;

    Result := TRUE;
  except
    on e: Exception do
    begin
      HTTPStatus := httpClient.ResponseStatusText;
      Result := FALSE;
      httpClient.Free;
      Exit;
    end;
  end;
  httpClient.Free;
end;

class function TUtil.isCNPJ(CNPJ: string): boolean;
var   dig13, dig14: string;
    sm, i, r, peso: integer;
begin
// length - retorna o tamanho da string do CNPJ (CNPJ é um número formado por 14 dígitos)
  if ((CNPJ = '00000000000000') or (CNPJ = '11111111111111') or
      (CNPJ = '22222222222222') or (CNPJ = '33333333333333') or
      (CNPJ = '44444444444444') or (CNPJ = '55555555555555') or
      (CNPJ = '66666666666666') or (CNPJ = '77777777777777') or
      (CNPJ = '88888888888888') or (CNPJ = '99999999999999') or
      (length(CNPJ) <> 14))
     then begin
            isCNPJ := false;
            exit;
          end;

// "try" - protege o código para eventuais erros de conversão de tipo através da função "StrToInt"
  try
{ *-- Cálculo do 1o. Digito Verificador --* }
    sm := 0;
    peso := 2;
    for i := 12 downto 1 do
    begin
// StrToInt converte o i-ésimo caractere do CNPJ em um número
      sm := sm + (StrToInt(CNPJ[i]) * peso);
      peso := peso + 1;
      if (peso = 10)
         then peso := 2;
    end;
    r := sm mod 11;
    if ((r = 0) or (r = 1))
       then dig13 := '0'
    else str((11-r):1, dig13); // converte um número no respectivo caractere numérico

{ *-- Cálculo do 2o. Digito Verificador --* }
    sm := 0;
    peso := 2;
    for i := 13 downto 1 do
    begin
      sm := sm + (StrToInt(CNPJ[i]) * peso);
      peso := peso + 1;
      if (peso = 10)
         then peso := 2;
    end;
    r := sm mod 11;
    if ((r = 0) or (r = 1))
       then dig14 := '0'
    else str((11-r):1, dig14);

{ Verifica se os digitos calculados conferem com os digitos informados. }
    if ((dig13 = CNPJ[13]) and (dig14 = CNPJ[14]))
       then isCNPJ := true
    else isCNPJ := false;
  except
    isCNPJ := false
  end;
end;


end.

