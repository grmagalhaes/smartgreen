unit Principal;

(* Criado por Gerson Rodrigues Magalhães
   Avaliação Stone :: Client/Server de uma Transação de Pagamentos *)

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, StdCtrls, Buttons, Card, Merchant, fpsTypes, lclintf;

type

  { TApplicationForm }

  TApplicationForm = class(TForm)
    AcceptRadioGroup: TRadioGroup;
    ApplyButton: TBitBtn;
    BalanceEdit: TEdit;
    BlockedCheckList: TCheckBox;
    BrandComboBox: TComboBox;
    BrandLabel2: TLabel;
    CardHolderNameEdit: TEdit;
    CardHorderNameLabel: TLabel;
    CardHorderNameLabel1: TLabel;
    CardNumberEdit: TEdit;
    CardNumberLabel: TLabel;
    CurrencyComboBox: TComboBox;
    CurrencyLabel: TLabel;
    ExpirationDateEdit: TEdit;
    ExpirationDateLabel: TLabel;
    ExpirationDateLabel1: TLabel;
    ExpirationDateLabel2: TLabel;
    HasPassword: TCheckBox;
    Label1: TLabel;
    StoneCodeLabel: TLabel;
    SerialLabel: TLabel;
    LimitEdit: TEdit;
    LostCheckList: TCheckBox;
    MerchantAddressEdit: TEdit;
    MerchantAddressLabel: TLabel;
    MerchantApplyButton: TBitBtn;
    MerchantCityEdit: TEdit;
    MerchantCityLabel1: TLabel;
    MerchantCNPJEdit: TEdit;
    MerchantStoneCodeEdit: TEdit;
    MerchantCNPJLabel: TLabel;
    MerchantCodeEdit: TEdit;
    MerchantCodeLabel: TLabel;
    MerchantNameEdit: TEdit;
    MerchantNameLabel: TLabel;
    MerchantStateEdit: TEdit;
    MerchantStateLabel: TLabel;
    PasswordEdit1: TEdit;
    RobbedCheckList: TCheckBox;
    SmartGreenPageControl: TPageControl;
    MerchantSerialEdit: TEdit;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TypeRadioGroup: TRadioGroup;
    procedure CadastrarEmpresaButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure LimitEditKeyPress(Sender: TObject; var Key: char);
    procedure CardNumberEditKeyUp(Sender: TObject);
    procedure CadastrarCartaoButtonClick(Sender: TObject);
    procedure ApplyButtonClick(Sender: TObject);
    procedure EnterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MerchantApplyButtonClick(Sender: TObject);
    procedure OnlyLetterKeyPress(Sender: TObject; var Key: char);
    procedure OnlyNumberKeyPress(Sender: TObject; var Key: char);
    procedure OnlyNumberSlashKeyPress(Sender: TObject; var Key: char);
  public
    procedure ZeraCamposCard;
    procedure ZeraCamposMerchant;
  end;

var
  ApplicationForm: TApplicationForm;

implementation

{$R *.lfm}

{ TApplicationForm }

procedure TApplicationForm.ApplyButtonClick(Sender: TObject);
var Card : TCard;
    FloatTmp : Double;
begin
  try
    Card := TCard.Create;
    Card.SetNumber(CardNumberEdit.Text);
    Card.SetCardBrand(BrandCombobox.Items[BrandComboBox.ItemIndex]);
    Card.SetCardHolderName(CardHolderNameEdit.Text);

    Card.SetPassword(PasswordEdit1.Text);
    Card.SetPasswordSize(Length(PasswordEdit1.Text));

    Card.SetExpirationDate(ExpirationDateEdit.Text);

    if (TypeRadioGroup.ItemIndex = 0) then
      Card.SetCardType('C')
    else
      Card.SetCardType('T');

    case AcceptRadioGroup.ItemIndex of
      0: Card.SetAccept('C');
      1: Card.SetAccept('D');
      2: Card.SetAccept('CD');
      3: Card.SetAccept('V');
    end;

    Card.SetCurrency(CurrencyComboBox.Items[CurrencyComboBox.ItemIndex]);
    Card.SetHasPassword(HasPassword.Checked);

    Card.SetRobbed(RobbedCheckList.Checked);
    Card.SetLost(LostCheckList.Checked);
    Card.SetBlocked(BlockedCheckList.Checked);

    DefaultFormatSettings.DecimalSeparator := '.';
    DefaultFormatSettings.ThousandSeparator := ',';

    if (LimitEdit.Text = '') then LimitEdit.Text := '1000.00';
    FloatTmp := StrtoFloat(LimitEdit.Text);

    Card.SetLimit(FloatTmp);

    if (BalanceEdit.Text = '') then BalanceEdit.Text := '0.00';
    FloatTmp := StrtoFloat(BalanceEdit.Text);
    Card.SetBalance(FloatTmp);

    if (Card.Validate) then
    begin
      if (Card.CreateCard) then
      begin
        ShowMessage('Cartão criado com sucesso');
      end
      else
      begin
        ShowMessage('Erro ao criar o cartão');
      end;
    end
    else
      ShowMessage(Card.GetValidateMessage);

  finally
    Card.Free;
  end;
end;

procedure TApplicationForm.EnterClick(Sender: TObject);
begin
end;

procedure TApplicationForm.FormCreate(Sender: TObject);
begin
  SmartGreenPageControl.ActivePage := TabSheet1;
end;

procedure TApplicationForm.MerchantApplyButtonClick(Sender: TObject);
var Merchant : TMerchant;

begin
  try
    Merchant := TMerchant.Create;

    Merchant.SetId(MerchantCodeEdit.Text);
    Merchant.SetName(MerchantNameEdit.Text);
    Merchant.SetAddress(MerchantAddressEdit.Text);
    Merchant.SetCity(MerchantCityEdit.Text);
    Merchant.SetState(MerchantStateEdit.Text);

    Merchant.SetCNPJ(MerchantCNPJEdit.Text);

    Merchant.SetSerialNumber(MerchantSerialEdit.Text);
    Merchant.SetStoneCode(MerchantStoneCodeEdit.Text);

    if (Merchant.Validate) then
    begin
      if (Merchant.CreateMerchant) then
      begin
        ShowMessage('Estabelecimento criado com sucesso');
      end
      else
      begin
        ShowMessage('Erro ao criar o estabelecimento');
      end;
    end
    else
      ShowMessage(Merchant.GetValidateMessage);

  finally
    Merchant.Free;
  end;
end;

procedure TApplicationForm.OnlyLetterKeyPress(Sender: TObject; var Key: char);
begin
  Key := UpCase(Key);
  if (Key in ['0'..'9']) then Key := #0;
end;

procedure TApplicationForm.OnlyNumberKeyPress(Sender: TObject; var Key: char);
begin
  if not(Key in ['0'..'9', #9, #8]) then Key := #0;
end;

procedure TApplicationForm.OnlyNumberSlashKeyPress(Sender: TObject;
  var Key: char);
begin
  if not(Key in ['0'..'9', '/', #9, #8]) then Key := #0;
end;

procedure TApplicationForm.CadastrarEmpresaButtonClick(Sender: TObject);
begin
  // Zera campos
  ZeraCamposMerchant;

  // Habilita aba de cartões e desabilita a de CRUD
  MerchantCodeEdit.SetFocus;

end;

procedure TApplicationForm.CancelButtonClick(Sender: TObject);
begin
end;

// Filtro para impedir que valores não numéricos (além do ponto) sejam utilizados nos campos gasto e limite
procedure TApplicationForm.LimitEditKeyPress(Sender: TObject; var Key: char);
begin
    if not(Key in ['0'..'9', '.', #9, #8]) then Key := #0;
    if (Key = '.')  and (Pos('.', (Sender as TEdit).Caption) <> 0) then Key := #0;
end;

 procedure TApplicationForm.CardNumberEditKeyUp(Sender: TObject);
var CardList : TCardList;
    Brand : String;

begin
  // Rotina para descobrir o cartão e preencher os campos de acordo

  Brand := TCard.GetBrandByCardNumber(Trim(CardNumberEdit.Text));
  CardList := TCard.GetDefaultAttributesByCardBrand(Brand);

  if (CardList.CardBrand <> '') then
  begin
    // Encontrou valores padroes
    BrandComboBox.ItemIndex := BrandComboBox.Items.IndexOf(CardList.CardBrand);

    if (CardList.CardType = 'C') then
      TypeRadioGroup.ItemIndex := 0
    else
      TypeRadioGroup.ItemIndex := 1;

    if (CardList.Accept = 'C') then  AcceptRadioGroup.ItemIndex := 0;
    if (CardList.Accept = 'D') then  AcceptRadioGroup.ItemIndex := 1;
    if (CardList.Accept = 'CD') then  AcceptRadioGroup.ItemIndex := 2;
    if (CardList.Accept = 'V') then  AcceptRadioGroup.ItemIndex := 3;

    HasPassword.Checked := CardList.HasPassword;
  end
  else
  begin
    // Nao e uma bandeira aceita. Mantem a bandeira mas o restante fica o valor default
    BrandComboBox.ItemIndex := BrandComboBox.Items.IndexOf(Brand);
    TypeRadioGroup.ItemIndex := 0;
    AcceptRadioGroup.ItemIndex := 0;
    HasPassword.Checked := TRUE;
  end;
end;

procedure TApplicationForm.CadastrarCartaoButtonClick(Sender: TObject);
begin
  // Zera campos
  ZeraCamposCard;

  SmartGreenPageControl.ActivePage := TabSheet1;
  CardNumberEdit.SetFocus;

end;

procedure TApplicationForm.ZeraCamposCard;
begin
    // Zera campos da tela de cartões
  CardNumberEdit.Text := '';
  BrandCombobox.ItemIndex := 0;
  CardHolderNameEdit.Text := '';
  ExpirationDateEdit.Text := '';
  TypeRadioGroup.ItemIndex := 0;
  AcceptRadioGroup.ItemIndex := 0;
  CurrencyComboBox.ItemIndex := 0;
  HasPassword.Checked := TRUE;
end;

procedure TApplicationForm.ZeraCamposMerchant;
begin
    // Zera campos da tela de empresas
  MerchantCodeEdit.Text := '';
  MerchantNameEdit.Text := '';
  MerchantAddressEdit.Text := '';
  MerchantCityEdit.Text := '';
  MerchantStateEdit.Text := '';
  MerchantCNPJEdit.Text := '';
  MerchantStoneCodeEdit.Text := '';
  MerchantSerialEdit.Text := '';

end;


end.

