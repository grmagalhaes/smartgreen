unit merchant;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, ComCtrls, Controls, fpjson, jsonparser, inifiles;

type

  { TCard. A classe de empresas. }

  TMerchant = class
  private
    ValidateMessage : String;
    Message : String;

    Id : String;
    Name : String;
    Address : String;
    City : String;
    State : String;
    CNPJ : String;

    SerialNumber : String;
    StoneCode : String;

    URL : String;

    function CreateJSON : String;

  public
   function GetMessage : String;

   function GetValidateMessage : String;
   function Validate : Boolean;

   procedure SetId(S : String);
   function GetId : String;

    procedure SetName(S : String);
    function GetName : String;

    procedure SetAddress(S: String);
    function GetAddress : String;

    procedure SetCity(S: String);
    function GetCity : String;

    procedure SetState(S: String);
    function GetState : String;

    procedure SetCNPJ(S: String);
    function GetCNPJ : String;

    procedure SetSerialNumber(S : String);
    function GetSerialNumber : String;

    procedure SetStoneCode(S : String);
    function GetStoneCode : String;

    function CreateMerchant : Boolean;
    function DeleteMerchant : Boolean;

end;

implementation

uses Util;

// última mensagem da classe após um comando
function TMerchant.GetMessage : String;
begin
    Result := Message;
end;

// rotina que valida as informações de um cartão. São várias regras, tal como
// tamanho, nome do usuário, bandeira, formato da data de expiração, tipo,
// se é crédito/débito, moeda
function TMerchant.Validate : Boolean;
begin
  Result := TRUE;
  ValidateMessage := '';

  if (GetCNPJ <> '') then
  begin
    if (not TUtil.isCNPJ(GetCNPJ)) then
      ValidateMessage := 'CNPJ Inválido' + #13#10;
  end;


  if (ValidateMessage <> '') then Result := FALSE;

end;

// mensagem de erro de validação. Ela acumula vários erros
function TMerchant.GetValidateMessage : String;
begin
  Result := ValidateMessage;
end;

//getter/setter de id da empresa
procedure TMerchant.SetId(S : String);
begin
  Id := Trim(S);
end;

function TMerchant.GetId : String;
begin
  Result := Id;
end;

// getter/setter de nome da empresa
procedure TMerchant.SetName(S : String);
begin
    Name := Trim(S);
end;

function TMerchant.GetName : String;
begin
  Result := Name;
end;

// getter/setter do endereço
procedure TMerchant.SetAddress(S: String);
begin
    Address := Trim(S);
end;

function TMerchant.GetAddress : String;
begin
   Result := Address;
end;

// getter/setter da cidade
procedure TMerchant.SetCity(S: String);
begin
    City := S;
end;

function TMerchant.GetCity : String;
begin
    Result := City;
end;

// getter/setter do estado
procedure TMerchant.SetState(S: String);
begin
    State := Trim(S)
end;

function TMerchant.GetState : String;
begin
  Result := State;
end;

// getter/setter de cnpj
procedure TMerchant.SetCNPJ(S: String);
begin
    CNPJ := Trim(S);
end;

function TMerchant.GetCNPJ : String;
begin
  Result := CNPJ;
end;

// Serial Number do Equipamento
procedure TMerchant.SetSerialNumber(S : String);
begin
  SerialNumber := S;
end;

function TMerchant.GetSerialNumber : String;
begin
  Result := SerialNumber;
end;

// Codigo Stone do Equipamento
procedure TMerchant.SetStoneCode(S : String);
begin
  StoneCode := S;
end;

function TMerchant.GetStoneCode : String;
begin
  Result := StoneCode;
end;

// criação de JSON de uma empresa
function TMerchant.CreateJSON : String;
var JSONText : String;
begin

  JSONText := JSONText + '{';
  JSONText := JSONText +   '"merchant":';
  JSONText := JSONText +     '{';
  JSONText := JSONText +       '"id":"' + Self.GetId + '",';
  JSONText := JSONText +       '"name":"' + Self.GetName + '",';
  JSONText := JSONText +       '"address":"' + Self.GetAddress + '",';
  JSONText := JSONText +       '"city":"' + Self.GetCity + '",';
  JSONText := JSONText +       '"state":"' + Self.GetState + '",';
  JSONText := JSONText +       '"cnpj":"' + Self.GetCNPJ + '",';
  JSONText := JSONText +       '"serial_number":"' + Self.GetSerialNumber + '",';
  JSONText := JSONText +       '"stone_code":"' + Self.GetStoneCode + '"';
  JSONText := JSONText +     '}}';

  Result := JSONText;

end;

// rotina de criação de um empreendimento
function TMerchant.CreateMerchant : Boolean;
var
  HTTPRequest : String;
  HTTPStatus : String;
  HTTPResponse : String;
  Ret : Boolean;
  jData : TJSONData;
  jObject : TJSONObject;
  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'config.ini');
  URL := IniFile.ReadString('SERVICE', 'URL', 'http://localhost:8080/sgs');
  IniFile.Free;


  HTTPRequest := CreateJSON;
  HTTPStatus := '';
  HTTPResponse := '';

  Ret := TUtil.HTTPSend(URL + '/createmerchant', HTTPRequest, HTTPStatus, HTTPResponse);
  if (HTTPResponse <> '') then
  begin
    if Ret then
    begin
      jData := GetJSON(HTTPResponse);
      jObject := TJSONObject(jData);
      if (LowerCase(jObject.Get('result')) = 'true') then Result := TRUE else Result := FALSE;
    end
    else
      Result := FALSE;
  end
  else
  Result := FALSE;
end;

// rotina de remoção de empresa
function TMerchant.DeleteMerchant : Boolean;
var
  HTTPRequest : String;
  HTTPStatus : String;
  HTTPResponse : String;
  Ret : Boolean;
  jData : TJSONData;
  jObject : TJSONObject;
  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'config.ini');
  URL := IniFile.ReadString('SERVICE', 'URL', 'http://localhost:8080/sgs');
  IniFile.Free;

  HTTPRequest := '{"merchant":{"id":"' + Self.GetId + '"}}';
  HTTPStatus := '';
  HTTPResponse := '';

  Ret := TUtil.HTTPSend(URL + '/deletemerchant', HTTPRequest, HTTPStatus, HTTPResponse);

  if Ret then
  begin
    jData := GetJSON(HTTPResponse);
    jObject := TJSONObject(jData);
    if (LowerCase(jObject.Get('result')) = 'true') then Result := TRUE else Result := FALSE;
  end
  else
    Result := FALSE;
end;



end.

