program SmartGreenAdmin;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Principal, card, Util, laz_fpspreadsheet, merchant;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TApplicationForm, ApplicationForm);
//  Application.CreateForm(TTransaction, Transaction);
//  Application.CreateForm(TCard1, Card1);
  Application.Run;
end.

