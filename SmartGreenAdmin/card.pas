unit Card;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, ComCtrls, Controls, fpjson, jsonparser, inifiles, DCPSHA256;

type

  TCardList = record
                 CardBrand : String;
                 CardType : String;
                 Accept : String;
                 HasPassword : Boolean;
               end;

  { TCard. A classe de cartões. Usada tanto no cadastro de cartões quanto no envio de transações }

  TCard = class
    ImageList1: TImageList;
    procedure DataModuleCreate(Sender: TObject);
  private
    ValidateMessage : String;
    Message : String;

    Number : String;
    CardHolderName : String;
    ExpirationDate : String;
    CardBrand : String;
    Password : String;
    PasswordSize : Integer;
    CardType : String;
    Accept : String;
    HasPassword : Boolean;
    Currency : String;

    Limit : Double;
    Balance : Double;
    Robbed : Boolean;
    Lost : Boolean;
    Blocked : Boolean;

    MessageCode : String;
    URL : String;

    function CreateJSON : String;

  public
    class function GetBrandByCardNumber(S : String) : String;
    class function GetDefaultAttributesByCardBrand(S: String) : TCardList;
    function IsAccepted(Brand: String) : Boolean;

    function GetMessage : String;

    function Validate : Boolean;
    function GetValidateMessage : String;

    procedure SetNumber(N : String);
    function GetNumber : String;

    procedure SetCardBrand(N : String);
    function GetCardBrand : String;

    procedure SetCardHolderName(S: String);
    function GetCardHolderName : String;

    procedure SetExpirationDate(S: String);
    function GetExpirationDate : String;

    procedure SetCardType(S: String);
    function GetCardType : String;

    procedure SetAccept(S: String);
    function GetAccept : String;

    procedure SetPassword(S : String);
    function GetPassword : String;

    procedure SetPasswordSize(N : Integer);
    function GetPasswordSize : Integer;

    procedure SetHasPassword(B : Boolean);
    function GetHasPassword : Boolean;

    procedure SetCurrency(S : String);
    function GetCurrency : String;

    procedure SetLimit(D : Double);
    function GetLimit : Double;

    function GetBalance : Double;
    procedure SetBalance(D : Double);

    function GetRobbed : Boolean;
    procedure SetRobbed(B : Boolean);

    function GetLost : Boolean;
    procedure SetLost(B : Boolean);

    function GetBlocked : Boolean;
    procedure SetBlocked(B : Boolean);

    function CreateCard : Boolean;
    function DeleteCard : Boolean;

end;

const

  (* Lista de cartões aceitos pela Stone *)
  MAXCARD = 14;
  CardList : array[1..MAXCARD] of TCardList = (
  (CardBrand: 'VISA';          CardType:'C'; Accept:'C'; HasPassword:TRUE),
  (CardBrand: 'VISA ELECTRON'; CardType:'C'; Accept:'D'; HasPassword:TRUE),
  (CardBrand: 'MASTERCARD';    CardType:'C'; Accept:'C'; HasPassword:TRUE),
  (CardBrand: 'MAESTRO';       CardType:'C'; Accept:'D'; HasPassword:TRUE),
  (CardBrand: 'ELO';           CardType:'C'; Accept:'D'; HasPassword:TRUE),
  (CardBrand: 'HIPERCARD';     CardType:'C'; Accept:'D'; HasPassword:TRUE),
  (CardBrand: 'HIPER';         CardType:'C'; Accept:'D'; HasPassword:TRUE),
  (CardBrand: 'AMEX';          CardType:'T'; Accept:'C'; HasPassword:FALSE),
  (CardBrand: 'VR';            CardType:'C'; Accept:'V'; HasPassword:TRUE),
  (CardBrand: 'ALELO';         CardType:'C'; Accept:'V'; HasPassword:TRUE),
  (CardBrand: 'COOPER';        CardType:'C'; Accept:'V'; HasPassword:TRUE),
  (CardBrand: 'TICKET';        CardType:'C'; Accept:'V'; HasPassword:TRUE),
  (CardBrand: 'SODEXO';        CardType:'C'; Accept:'V'; HasPassword:TRUE),
  (CardBrand: 'GREENCARD';     CardType:'C'; Accept:'V'; HasPassword:TRUE));

implementation

uses Util;

procedure TCard.DataModuleCreate(Sender: TObject);
begin
  MessageCode := '';
end;

// uma função de class (não precisa de instância) que retorna a bandeira do cartão dado um número
class function TCard.GetBrandByCardNumber(S : String) : String;
var Number6, Len : Longint;
begin
  S := Trim(S);
  Len := Length(S);

  if (S = '') or (Len < 13) then
  begin
    Result := '';
    Exit;
  end;

  Number6 := StrtoIntDef(Copy(S, 1, 6), 0);

  Result := 'DESCONHECIDO';
  if ((Number6 >= 060400) and (Number6 <= 060699)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 222100) and (Number6 <= 272099)) and (Len = 16) then Result := 'MASTERCARD';
  if ((Number6 >= 300000) and (Number6 <= 305999)) and ((Len = 14) or (Len = 16)) then Result := 'DINERS';
  if ((Number6 >= 340000) and (Number6 <= 349999)) and (Len = 15) then Result := 'AMEX';
  if ((Number6 >= 350000) and (Number6 <= 359999)) and (Len = 16) then Result := 'JCB';
  if ((Number6 >= 360000) and (Number6 <= 362899)) and ((Len = 14) or (Len = 16)) then Result := 'DINERS';
  if ((Number6 >= 362900) and (Number6 <= 362999)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 363000) and (Number6 <= 369999)) and ((Len = 14) or (Len = 16)) then Result := 'DINERS';
  if ((Number6 >= 370000) and (Number6 <= 379999)) and (Len = 15) then Result := 'AMEX';
  if ((Number6 >= 380000) and (Number6 <= 389999)) and ((Len = 14) or (Len = 16)) then Result := 'DINERS';
  if ((Number6 >= 384100) and (Number6 <= 384199)) and ((Len = 13) or (Len = 16) or (Len = 19)) then Result := 'HIPERCARD';
  if ((Number6 >= 400000) and (Number6 <= 499999)) and ((Len = 13) or (Len = 16)) then Result := 'VISA';
  if ((Number6 >= 401100) and (Number6 <= 401199)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 402600) and (Number6 <= 402699)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 417500) and (Number6 <= 417599)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 431274) and (Number6 <= 431274)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 438935) and (Number6 <= 438935)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 440500) and (Number6 <= 440599)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 450800) and (Number6 <= 450899)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 451416) and (Number6 <= 451416)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 457600) and (Number6 <= 457699)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 457631) and (Number6 <= 457632)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 457913) and (Number6 <= 457913)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 484400) and (Number6 <= 484499)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 491300) and (Number6 <= 491399)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 491700) and (Number6 <= 491799)) and ((Len = 13) or (Len = 16)) then Result := 'VISA ELECTRON';
  if ((Number6 >= 500000) and (Number6 <= 599999)) and (Len = 16) then Result := 'MASTERCARD';
  if ((Number6 >= 500000) and (Number6 <= 509999)) and ((Len = 16) or (Len = 19)) then Result := 'AURA';
  if ((Number6 >= 501800) and (Number6 <= 501899)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 501900) and (Number6 <= 501999)) and (Len = 16) then Result := 'DANKORT';
  if ((Number6 >= 502000) and (Number6 <= 502199)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 503800) and (Number6 <= 503899)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 504175) and (Number6 <= 504175)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 506699) and (Number6 <= 506799)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 506712) and (Number6 <= 506712)) and (Len = 16) then Result := 'ALELO';
  if ((Number6 >= 509000) and (Number6 <= 509999)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 510000) and (Number6 <= 559999)) and (Len = 16) then Result := 'MASTERCARD';
  if ((Number6 >= 560000) and (Number6 <= 589999)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 561200) and (Number6 <= 561299)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 590000) and (Number6 <= 599999)) and (Len = 16) then Result := 'MASTERCARD';
  if ((Number6 >= 599300) and (Number6 <= 589399)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 600000) and (Number6 <= 609999)) and ((Len = 13) or (Len = 16) or (Len = 19)) then Result := 'HIPERCARD';
  if ((Number6 >= 600818) and (Number6 <= 600818)) and (Len = 16) then Result := 'SODEXO';
  if ((Number6 >= 601100) and (Number6 <= 601199)) and (Len = 16) then Result := 'DISCOVER';
  if ((Number6 >= 602651) and (Number6 <= 602651)) and (Len = 16) then Result := 'TICKET';
  if ((Number6 >= 603389) and (Number6 <= 603389)) and (Len = 16) then Result := 'SODEXO';
  if ((Number6 >= 605676) and (Number6 <= 605676)) and (Len = 16) then Result := 'TICKET';
  if ((Number6 >= 606068) and (Number6 <= 606071)) and (Len = 16) then Result := 'SODEXO';
  if ((Number6 >= 606282) and (Number6 <= 606282)) and ((Len = 13) or (Len = 16) or (Len = 19)) then Result := 'HIPERCARD';
  if ((Number6 >= 606303) and (Number6 <= 606303)) and (Len = 16) then Result := 'GREENCARD';
  if ((Number6 >= 620000) and (Number6 <= 629999)) then Result := 'UNION PAY';
  if ((Number6 >= 622000) and (Number6 <= 622999)) and (Len = 16) then Result := 'DISCOVER';
  if ((Number6 >= 627780) and (Number6 <= 627780)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 630400) and (Number6 <= 630499)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 633400) and (Number6 <= 633499)) then Result := 'SOLO';
  if ((Number6 >= 635400) and (Number6 <= 635499)) and (Len = 16) then Result := 'VR';
  if ((Number6 >= 636000) and (Number6 <= 636999)) and (Len = 16) then Result := 'INTERPAYMENT';
  if ((Number6 >= 636297) and (Number6 <= 636297)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 636368) and (Number6 <= 636369)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 636505) and (Number6 <= 636505)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 637036) and (Number6 <= 637036)) and (Len = 16) then Result := 'VR';
  if ((Number6 >= 639000) and (Number6 <= 639099)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 640000) and (Number6 <= 659999)) and (Len = 16) then Result := 'DISCOVER';
  if ((Number6 >= 650031) and (Number6 <= 650033)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650035) and (Number6 <= 650051)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650405) and (Number6 <= 650439)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650485) and (Number6 <= 650538)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650541) and (Number6 <= 650598)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650700) and (Number6 <= 650718)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650720) and (Number6 <= 650727)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 650901) and (Number6 <= 650920)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 651652) and (Number6 <= 651679)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 655000) and (Number6 <= 655019)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 655021) and (Number6 <= 655058)) and (Len = 16) then Result := 'ELO';
  if ((Number6 >= 675900) and (Number6 <= 675999)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 676100) and (Number6 <= 676399)) and (Len = 16) then Result := 'MAESTRO';
  if ((Number6 >= 676700) and (Number6 <= 676799)) then Result := 'SOLO';
  if ((Number6 >= 880000) and (Number6 <= 889999)) then Result := 'UNION PAY';
end;

// função de classe que retorna valores padrões para uma determinada bandeira.
// tais informações podem ser alteradas na inclusão de um cartão
class function TCard.GetDefaultAttributesByCardBrand(S: String) : TCardList;
var I : Integer;
begin
  for I := 1 to MAXCARD do
    if (CardList[I].CardBrand = S) then
    begin
      Result := CardList[I];
      Break;
    end;
end;

// verifica se um cartão é aceito pela Stone, ou seja, se está na CartList acima
function TCard.IsAccepted(Brand: String) : Boolean;
var I : Integer;
begin
  Result := FALSE;
  for I := 1 to MAXCARD do
  begin
      if (CardList[I].CardBrand = Brand) then
      begin
        Result := TRUE;
        Exit;
      end;
  end;
end;

// última mensagem da classe após um comando
function TCard.GetMessage : String;
begin
    Result := Message;
end;

// rotina que valida as informações de um cartão. São várias regras, tal como
// tamanho, nome do usuário, bandeira, formato da data de expiração, tipo,
// se é crédito/débito, moeda
function TCard.Validate : Boolean;
begin
  Result := TRUE;
  ValidateMessage := '';

  if (Length(Number) < 13) or (Length(Number) > 19) then
    ValidateMessage := 'Número do cartão inválido' + #13#10;

  if(Length(Trim(CardHolderName)) = 0) then
    ValidateMessage := ValidateMessage + 'O nome do cliente é obrigatório' + #13#10;

  if (CardBrand = 'DESCONHECIDO') then
     ValidateMessage := ValidateMessage + 'Uma bandeira deverá ser escolhida' + #13#10;

  if not ( (Length(ExpirationDate) = 5) and (Copy(ExpirationDate, 3, 1) = '/') and
     ( (Copy(ExpirationDate, 1, 1) >= '0') and (Copy(ExpirationDate, 1, 1) <= '9') ) and
     ( (Copy(ExpirationDate, 2, 1) >= '0') and (Copy(ExpirationDate, 2, 1) <= '9') ) and
     ( (Copy(ExpirationDate, 4, 1) >= '0') and (Copy(ExpirationDate, 4, 1) <= '9') ) ) then
    ValidateMessage := ValidateMessage + 'Data de validade inválida'+ #13#10;

  if (StrtoIntDef(Copy(ExpirationDate, 1, 2), 0) < 1) or (StrtoIntDef(Copy(ExpirationDate, 1, 2), 0) > 12) then
    ValidateMessage := ValidateMessage + 'Data de validade inválida'+ #13#10;

  if not ((CardType = 'C') or (CardType = 'T')) then
    ValidateMessage := ValidateMessage + 'Tipo inválido'+ #13#10;

  if not ( (Accept = 'C') or (Accept = 'D') or (Accept = 'CD') or (Accept = 'V') ) then
    ValidateMessage := ValidateMessage + 'Tipo de bandeira inválida'+ #13#10;

  if ( (Currency <> 'BRL') and (Currency <> 'EUR') and (Currency <> 'USD')) then
    ValidateMessage := ValidateMessage + 'Moeda inválida';

  if (ValidateMessage <> '') then Result := FALSE;

end;

// mensagem de erro de validação. Ela acumula vários erros
function TCard.GetValidateMessage : String;
begin
  Result := ValidateMessage;
end;

//getter/setter de number
procedure TCard.SetNumber(N : String);
begin
    Number := Trim(N);
    CardBrand := GetBrandByCardNumber(Number);
end;

function TCard.GetNumber : String;
begin
  Result := Number;
end;

// getter/setter de bandeira
procedure TCard.SetCardBrand(N : String);
begin
    CardBrand := Trim(N);
end;

function TCard.GetCardBrand : String;
begin
  Result := CardBrand;
end;

// getter/setter de usuário
procedure TCard.SetCardHolderName(S: String);
begin
    CardHolderName := Trim(S);
end;

function TCard.GetCardHolderName : String;
begin
   Result := CardHolderName;
end;

// getter/setter de data de validade
procedure TCard.SetExpirationDate(S: String);
begin
    ExpirationDate := S;
end;

function TCard.GetExpirationDate : String;
begin
    Result := ExpirationDate;
end;

// getter/setter do tipo físico (chip ou tarja)
procedure TCard.SetCardType(S: String);
begin
    CardType := Copy(UpperCase(S),1 , 1);
end;

function TCard.GetCardType : String;
begin
  Result := CardType;
end;

// getter/setter se a bandeira é aceita pela stone
procedure TCard.SetAccept(S: String);
begin
    Accept := UpperCase(S);
end;

function TCard.GetAccept : String;
begin
  Result := Accept;
end;

// seta a senha. a mesma é armazenada codificada via SHA256
procedure TCard.SetPassword(S : String);
  var Sha256: TDCP_sha256;
  Sha256Out: array[0..31] of byte;
  i : integer;

begin
  for i:= 0 to 31 do Sha256Out[i] := 0;
  Sha256:= TDCP_sha256.Create(nil);
  Sha256.Init;
  Sha256.UpdateStr(S);
  Sha256.Final(Sha256Out);
  Password := TUtil.BinToHex(Sha256Out);
  SHA256.Free;
end;

// retorna a senha codificiada
function TCard.GetPassword : String;
begin
  Result := Password;
end;

// getter/setter do tamanho da senha.
procedure TCard.SetPasswordSize(N : Integer);
begin
  PasswordSize := N;
end;

function TCard.GetPasswordSize : Integer;
begin
  Result := PasswordSize;
end;

// getter/setter para identificar a obrigatoriedade de senha
procedure TCard.SetHasPassword(B : Boolean);
begin
    HasPassword := B;
end;

function TCard.GetHasPassword : Boolean;
begin
  Result := HasPassword;
end;

// getter/setter da moeda
procedure TCard.SetCurrency(S : String);
begin
    Currency := Trim(S);
end;

function TCard.GetCurrency : String;
begin
  Result := Currency;
end;

function TCard.GetLimit : Double;
begin
   Result := Limit;
end;

procedure TCard.SetLimit(D : Double);
begin
   Limit := D;
end;

function TCard.GetBalance : Double;
begin
   Result := Balance;
end;

procedure TCard.SetBalance(D : Double);
begin
   Balance := D;
end;

function TCard.GetRobbed : Boolean;
begin
   Result := Robbed;
end;

procedure TCard.SetRobbed(B : Boolean);
begin
   Robbed := B;
end;

function TCard.GetLost : Boolean;
begin
   Result := Lost;
end;

procedure TCard.SetLost(B : Boolean);
begin
   Lost := B;
end;

function TCard.GetBlocked : Boolean;
begin
   Result := Blocked;
end;

procedure TCard.SetBlocked(B : Boolean);
begin
   Blocked := B;
end;

// criação de JSON de um cartão
function TCard.CreateJSON : String;
var JSONText : String;
begin

  DefaultFormatSettings.ShortDateFormat := 'dd/mm/yyyy';
  DefaultFormatSettings.ShortDateFormat := 'dd/mm/yyyy';
  DefaultFormatSettings.DecimalSeparator := '.';
  DefaultFormatSettings.ThousandSeparator := ',';

  JSONText := JSONText + '{';
  JSONText := JSONText +   '"card":';
  JSONText := JSONText +     '{';
  JSONText := JSONText +       '"number":"' + Self.GetNumber + '",';
  JSONText := JSONText +       '"card_holder_name":"' + Self.GetCardHolderName + '",';
  JSONText := JSONText +       '"brand":"' + Self.GetCardBrand + '",';
  JSONText := JSONText +       '"expiration_date":"' + Self.GetExpirationDate + '",';
  JSONText := JSONText +       '"type":"' + Self.GetCardType + '",';
  JSONText := JSONText +       '"accept":"' + Self.GetAccept + '",';
  JSONText := JSONText +       '"has_password":"' + BooltoStr(Self.GetHasPassword, true) + '",';
  JSONText := JSONText +       '"currency":"' + Self.GetCurrency + '",';
  JSONText := JSONText +       '"password":"' + Self.GetPassword + '",';
  JSONText := JSONText +       '"password_size":' + InttoStr(Self.GetPasswordSize) + ',';
  JSONText := JSONText +       '"balance":' + StringReplace(FloattoStrF(Self.GetBalance, ffnumber, 10, 2), ',', '', [rfReplaceAll]) + ',';
  JSONText := JSONText +       '"limit":' + StringReplace(FloattoStrF(Self.GetLimit, ffnumber, 10, 2), ',', '', [rfReplaceAll]) + ',';
  JSONText := JSONText +       '"lost":"' + BooltoStr(Self.GetLost, true) + '",';
  JSONText := JSONText +       '"robbed":"' + BooltoStr(Self.GetRobbed, true) + '",';
  JSONText := JSONText +       '"blocked":"' + BooltoStr(Self.GetBlocked, true) + '"';
  JSONText := JSONText +     '}}';

  Result := JSONText;

end;

// rotina de criação de um cartão na base local
function TCard.CreateCard : Boolean;
var
  HTTPRequest : String;
  HTTPStatus : String;
  HTTPResponse : String;
  Ret : Boolean;
  jData : TJSONData;
  jObject : TJSONObject;
  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'config.ini');
  URL := IniFile.ReadString('SERVICE', 'URL', 'http://localhost:8080/sgs');
  IniFile.Free;

  HTTPRequest := CreateJSON;
  HTTPStatus := '';
  HTTPResponse := '';

  Ret := TUtil.HTTPSend(URL + '/createcard', HTTPRequest, HTTPStatus, HTTPResponse);

  if (HTTPResponse <> '') then
  begin
    if Ret then
    begin
      jData := GetJSON(HTTPResponse);
      jObject := TJSONObject(jData);
      if (LowerCase(jObject.Get('result')) = 'true') then Result := TRUE else Result := FALSE;
    end
    else
      Result := FALSE;
  end
  else
    Result := FALSE;

end;

// rotina de criação de um cartão na base local
function TCard.DeleteCard : Boolean;
var
  HTTPRequest : String;
  HTTPStatus : String;
  HTTPResponse : String;
  Ret : Boolean;
  jData : TJSONData;
  jObject : TJSONObject;

  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'config.ini');
  URL := IniFile.ReadString('SERVICE', 'URL', 'http://localhost:8080/sgs');
  IniFile.Free;

  HTTPRequest := '{"card":{"number":"' + Self.GetNumber + '"}}';
  HTTPStatus := '';
  HTTPResponse := '';

  Ret := TUtil.HTTPSend(URL + '/deletecard', HTTPRequest, HTTPStatus, HTTPResponse);

  if Ret then
  begin
    jData := GetJSON(HTTPResponse);
    jObject := TJSONObject(jData);
    if (LowerCase(jObject.Get('result')) = 'true') then Result := TRUE else Result := FALSE;
  end
  else
    Result := FALSE;
end;



end.

