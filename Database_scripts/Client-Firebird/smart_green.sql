/* SmartGreen 0.1  */
/* by Gerson       */
/* Created in 2018/09/03 */
/* Changed in 2018/09/03 */

create table card_ (
card_number         varchar(19) not null primary key,
card_holder_name    varchar(40) not null,
expiration_date     varchar(10) not null,
card_brand          varchar(30) not null,
card_type           char(1) default 'C' not null,   
card_accept         varchar(2) default 'D' not null ,
has_password        char(1) default 'S' not null,
currency            varchar(3) default 'BRL' not null,
check (card_type in ('C', 'T')), 
check (has_password in ('S', 'N')),
check (card_accept in ('C', 'D', 'CD', 'V'))
);

create index card1 on card_ (card_holder_name);
create index card2 on card_ (card_brand);

comment on table card_ is 'Tabela com o cadastro de cartoes do lado do cliente';
comment on column card_.card_number is 'Numero do cartao. Os numeros que sao impressos no cartao, podendo variar entre 12 a 19 digitos';
comment on column card_.card_holder_name is 'Nome do portador do cartao';
comment on column card_.expiration_date is 'Data de expiracao do cartao';
comment on column card_.card_brand is 'Bandeira do cartao';
comment on column card_.card_type is 'Tipo de cartao. (C)hip ou (T)arja magnetica';
comment on column card_.card_accept is 'O cartao aceita (C)redito, (D)ebito, (V)oucher ou (A)Ambos: Credito/Debito';
comment on column card_.has_password is 'Se o cartao possui senha. Apenas cartoes de tarja magnetica podem ter essa propriedade';
comment on column card_.currency is 'Moeda do cartao';

create table transaction_ (
authorization       varchar(16) not null primary key,
card_number         varchar(19) not null,
amount              numeric(9,2) not null,
currency            varchar(3) default 'BRL' not null,
transaction_type    char(1) default 'C' not null,
number_installments numeric(2) default 1 not null,
merchant_id         varchar(20) not null,
status              varchar(1) default '0' not null,
response            varchar(4),
transaction_ts      timestamp default current_timestamp,
password_           varchar(100),
password_size       numeric(2),
check (transaction_type in ('C', 'D')),
foreign key (card_number) references card_ (card_number) on update cascade 
);

create index transaction1 on transaction_ (card_number);
create index transaction2 on transaction_ (status);
create index transaction3 on transaction_ (transaction_ts);
create index transaction4 on transaction_ (response);
create index transaction5 on transaction_ (merchant_id);

comment on table transaction_ is 'Tabela contendo as transacoes, independente do status';
comment on column transaction_.card_number is 'Numero do cartao. Os numeros que sao impressos no cartao, podendo variar entre 12 a 19 digitos';
comment on column transaction_.amount is 'Valor da transacao';
comment on column transaction_.currency is 'Moeda da transacao';
comment on column transaction_.transaction_type is 'Tipo da transacao: (D)ebito ou (C)redito';
comment on column transaction_.number_installments is 'Numero de parcelas. transacao de credito parcelado';
comment on column transaction_.merchant_id is 'Identificador do estabelecimento';
comment on column transaction_.status is 'Status da transacao';
comment on column transaction_.response is 'Cdigo da resposta da transacao';
comment on column transaction_.authorization is 'Codigo da autorizacao da transacao';
comment on column transaction_.transaction_ts is 'Data e hora da da transacao';
comment on column transaction_.password_ is 'Senha a ser enviada, codificada com SHA-256';
comment on column transaction_.password_size is 'Tamanho da senha';




DECLARE EXTERNAL FUNCTION substr
    CSTRING(80), SMALLINT, SMALLINT
    RETURNS CSTRING(80) FREE_IT
    ENTRY_POINT 'IB_UDF_substr' MODULE_NAME 'ib_udf';

DECLARE EXTERNAL FUNCTION strlen
    CSTRING(32767)
    RETURNS INTEGER BY VALUE
    ENTRY_POINT 'IB_UDF_strlen' MODULE_NAME 'ib_udf';
    

