/* SmartGreen 0.1  */
/* by Gerson       */
/* Created in 2018/09/10 */
/* Changed in 2018/09/10 */

use smartgreen;

create table merchant_ (
id                  varchar(10) not null primary key,
name                varchar(50) not null,
address             varchar(50),
city                varchar(30),
state_              varchar(2),
cnpj                varchar(11)
);

create index merchant1 on merchant_ (name);

create table card_ (
card_number         varchar(19) not null primary key,
card_holder_name    varchar(40) not null,
expiration_date     varchar(10) not null,
card_brand          varchar(30) not null,
card_type           char(1) default 'C' not null,   
card_accept         varchar(2) default 'D' not null ,
has_password        char(1) default 'S' not null,
password_           varchar(100),
password_size       int(2),
currency            varchar(3) default 'BRL' not null,
credit_limit        decimal(10,2) not null,
balance             decimal(10,2) not null,
robbed              char(1) default 'N' not null,
lost                char(1) default 'N' not null,
blocked             char(1) default 'N' not null,
check (card_type in ('C', 'T')), 
check (has_password in ('S', 'N')),
check (robbed in ('S', 'N')),
check (lost in ('S', 'N')),
check (card_accept in ('C', 'D', 'CD', 'V'))
);

create index card1 on card_ (card_holder_name);
create index card2 on card_ (card_brand);

create table transaction_ (
authorization       varchar(16) not null primary key, 
card_number         varchar(19) not null,
amount              decimal(9,2) not null,
currency            varchar(3) default 'BRL' not null,
transaction_type    char(1) default 'C' not null,
number_installments int(2) default 1 not null,
merchant_id         varchar(20) not null,
status              varchar(1) default '0' not null,
response            varchar(4),
transaction_ts      timestamp default current_timestamp,
password_           varchar(100),
password_size       int(2),
check (transaction_type in ('C', 'D'))
);

create index transaction1 on transaction_ (card_number);
create index transaction2 on transaction_ (status);
create index transaction3 on transaction_ (transaction_ts);
create index transaction4 on transaction_ (response);
create index transaction5 on transaction_ (merchant_id);

create table equipment_ (
serial              varchar(20) not null primary key,
stone_code          varchar(20) not null,
merchant_id         varchar(10) not null,
foreign key (merchant_id) references merchant_ (id)
);

create index equipment1 on equipment_ (stone_code);
create index equipment21 on equipment_ (merchant_id);

; DD basico
insert into merchant_ values ('45671', 'SUPERMERCADOS ACME LTDA', '', 'RIO DE JANEIRO', 'RJ', '');
insert into card_  values ('1234567890123458', 'GERSON R MAGALHAES', '09/19', 'MASTERCARD', 'C', 'C', 'S', '8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92', 6, 'BRL', 5000, 0, 'N', 'N', 'N');
insert into equipment_ values ('1122334455667788', '43532801', '45671');
; senha 123456

; export/import
echo ===== exportando MySQL ======
mysqldump -uroot -porigine71 -x -e --default-character-set=utf8 smartgreen > /tmp/backup.sql

echo ===== importando MySQL ======
mysql -u root -porigine71 smartgreen
drop database smartgreen;
create database smartgreen charset utf8;
mysql -u root -porigine71 smartgreen < %base%\backup.sql
