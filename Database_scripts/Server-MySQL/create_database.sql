-- ==========================================================================================
-- Infobol                    
-- Created by Gerson Rodrigues Magalhaes
--
-- Database: MySQL 5.5 or higher

-- Script used to create a database
-- First revision: 10/11/2012
--  Last revision: 10/13/2012
--  ==========================================================================================

-- system echo ===============================================================================;
-- system echo Creating Database;
-- system echo ===============================================================================;

-- ==========================================================================================
-- Create database
-- ==========================================================================================
-- drop database smartgreen;
create database smartgreen character set utf8 collate utf8_general_ci;

-- ==========================================================================================
-- Creating user
-- ==========================================================================================

 grant usage on *.* to root@ identified by 'senha';
 grant all privileges on smartgreen.* to root;
 
 grant usage on *.* to smartgreen identified by 'senha';
 grant all privileges on smartgreen.* to smartgreen;
 
 
