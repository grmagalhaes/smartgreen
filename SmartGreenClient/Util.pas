unit Util;

// classe criada por Gerson Rodrigues - 31/08

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,lazutf8,lclintf;

type

{Minhas classes utilitárias *}

TSha256Array = array[0..31] of byte;

TUTil = class
  public
    class function CurrentExeDir() : String; static;
    class function StringToHex(S: String): String; static;
    class function HexToString(S: string): string; static;
    class function BinToHex(S: TSha256Array): string;
    class procedure AbrirPagina(URL : String);
    class function EncodeURLElement(S: String): String;
end;

implementation

class function TUtil.CurrentExeDir() : String;
begin
  {$IFDEF DARWIN}
  Result := ExtractFilePath(ParamStr(0)) + '../../../';
  {$ENDIF}
  {$IFDEF WINDOWS}
  Result := ExtractFilePath(ParamStr(0))
  {$ENDIF}
end;

class function TUtil.StringToHex(S: string): string;
var
  i: integer;

begin
  Result := '';

  for i := 1 to Length( S ) do
    Result := Result + IntToHex( Ord( S[i] ), 2 );
end;

class function TUtil.HexToString(S: string): string;
var
  i: integer;

begin
  Result := '';

  for i := 1 to Length( S ) do
  begin
    if ((i mod 2) = 1) then
      Result := Result + Chr( StrToInt( '0x' + Copy( S, i, 2 )));
  end;
end;

class function TUtil.BinToHex(S: TSha256Array): string;
var
  i: integer;

begin
  Result := '';

  for i := 0 to Length(S) - 1 do
    Result := Result + IntToHex( Ord( S[i] ), 2 );
end;

class procedure TUtil.AbrirPagina(URL : String);
begin
  if (Pos(' ', URL) <> 0) then URL := Copy(URL, 1, Pos(' ', URL) - 1);
  if (URL <> '') then
  begin
    if (Copy(URL, 1, 3) = 'www') then
      URL := 'http://' + URL;

    if (Copy(URL, 1, 4) = 'http') then
      OpenURL(EncodeURLElement(URL)) { *Converted from ShellExecute* }
    else
      OpenDocument(SystoUTF8(URL));
  end;
end;

class function TUtil.EncodeURLElement(S: String): String;
//const  NotAllowed = [ ';', '/', '?', ':', '@', '=', '&', '#', '+', '_', '<', '>',
//                      '"', '%', '{', '}', '|', '\', '^', '~', '[', ']', '`' ];

var i, O, l : Integer;
      h: string[2];
      P : PChar;
      c: AnsiChar;

begin
  l := Length(S);
  if (l=0) then Exit;
  SetLength(Result,l * 3);
  P := Pchar(Result);
  for I := 1 to L do
  begin
    C := S[i];
    O := Ord(c);
//    if (O<=$20) or (O>=$7F) or (c in NotAllowed) then
    if (O>=$7F) then
    begin
      P^ := '%';
      Inc(P);
      h := IntToHex(Ord(c), 2);
      p^ := h[1];
      Inc(P);
      p^ := h[2];
      Inc(P);
    end
    else
    begin
      P^ := c;
      Inc(p);
    end;
  end;
  SetLength(Result,P-PChar(Result));
end;


end.

