unit Principal;

(* Criado por Gerson Rodrigues Magalhães
   Avaliação Stone :: Client/Server de uma Transação de Pagamentos *)

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, StdCtrls, Buttons, About, Card, Transaction, fpspreadsheet, fpsTypes, lclintf;

type

  { Fases do processo de preenchimento de dados da máquina - POS - algumas são bypassadas dependendo do tipo de cartão }
  TFase = (FASE_AGUARDANDO, FASE_CREDITO_DEBITO, FASE_VALOR, FASE_PARCELA, FASE_SENHA, FASE_ENVIANDO, FASE_FINAL);

  { TApplicationForm. Este Formulário representa o equipamento - POS - e a parte de cadastro de cartões e visualização das transações
    O banco de dados local é um Firebird embutido (autocontido) se o ambiente for Windows (não precisa de instalação).
    Se for MacOS ou Linus um Firebird 2.5 deve ser instalado
    A conexão com o servidor opcionalmente poderá ser feita via SSL. Para isso as dlls (ou os so se Linus/Dawrin)
    deverão estar copiados na pasta do executável. O protocolo utilizado é uma espécie de REST com JSON
    O arquivo chip.ini simula as informações gravadas no chip tal como stoneCode, serialnumber, estabelecimento, etc. *}

  TApplicationForm = class(TForm)
    ExcelButton: TBitBtn;
    UpdateTransactionButton: TBitBtn;
    BitBtn3: TBitBtn;
    InserirButton: TBitBtn;
    CriarButton: TBitBtn;
    CurrencyComboBox: TComboBox;
    CurrencyLabel: TLabel;
    CancelButton: TBitBtn;
    ApplyButton: TBitBtn;
    CardHolderNameEdit: TEdit;
    HasPassword: TCheckBox;
    ExpirationDateEdit: TEdit;
    CardHorderNameLabel: TLabel;
    BrandComboBox: TComboBox;
    ExpirationDateLabel: TLabel;
    CreditoDebitoLabel: TLabel;
    CardNumberEdit: TEdit;
    CardNumberLabel: TLabel;
    BrandLabel2: TLabel;
    Panel2: TPanel;
    TransactionListView: TListView;
    MerchantPanel: TPanel;
    Panel1: TPanel;
    DetalhesButton: TSpeedButton;
    ImagesButton: TSpeedButton;
    TypeRadioGroup: TRadioGroup;
    TelaTopo: TPanel;
    AcceptRadioGroup: TRadioGroup;
    ValorEdit: TEdit;
    Image1: TImage;
    CardListView: TListView;
    Tela: TPanel;
    SmartGreenPageControl: TPageControl;
    POSPanel: TPanel;
    Button3: TSpeedButton;
    Button1: TSpeedButton;
    Button2: TSpeedButton;
    Button4: TSpeedButton;
    Button5: TSpeedButton;
    Button6: TSpeedButton;
    Button7: TSpeedButton;
    Button8: TSpeedButton;
    Button9: TSpeedButton;
    Button0: TSpeedButton;
    Ajuda: TSpeedButton;
    Atalhos: TSpeedButton;
    Cancel: TSpeedButton;
    Back: TSpeedButton;
    Enter: TSpeedButton;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    OnOffButton: TToggleBox;
    SenhaEdit: TEdit;
    ParcelaEdit: TEdit;
    ValorLabel: TLabel;
    SenhaLabel: TLabel;
    BrandLabel: TLabel;
    ParcelaLabel: TLabel;
    procedure AjudaClick(Sender: TObject);
    procedure AtalhosClick(Sender: TObject);
    procedure BackClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DetalhesButtonClick(Sender: TObject);
    procedure ExcelButtonClick(Sender: TObject);
    procedure InserirButtonClick(Sender: TObject);
    procedure CardListViewDblClick(Sender: TObject);
    procedure CardNumberEditKeyUp(Sender: TObject);
    procedure CriarButtonClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure CancelClick(Sender: TObject);
    procedure ApplyButtonClick(Sender: TObject);
    procedure EnterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OnlyLetterKeyPress(Sender: TObject; var Key: char);
    procedure OnlyNumberKeyPress(Sender: TObject; var Key: char);
    procedure OnlyNumberSlashKeyPress(Sender: TObject; var Key: char);
    procedure OnOffButtonChange(Sender: TObject);
    procedure ImagesButtonClick(Sender: TObject);
    procedure SmartGreenPageControlChange(Sender: TObject);
    procedure UpdateTransactionButtonClick(Sender: TObject);
  private
    IsOn : Boolean;
  public
    procedure SetTextoTela(Msg : String);
    procedure TelaOff;

    procedure DesligaCamposTela;
    procedure ZeraCamposCard;
    procedure PowerOn;
    procedure PowerOff;

    procedure SetBrand(Brand : String);
    procedure SetMerchant(Merchant : String);
    procedure CreditoDebitoOn;
    procedure CreditoDebitoOff;
    procedure ValorOn;
    procedure ValorOff;
    procedure ParcelaOn;
    procedure ParcelaOff;
    procedure SenhaOn;
    procedure SenhaOff;
    procedure CancelButton1;
    procedure PreencheValor;
    procedure PreencheParcela;
    procedure PreencheSenha;
    procedure Enviar;
  end;

var
  ApplicationForm: TApplicationForm;
  Taction : TTransaction;
  Fase    : TFase;

implementation

uses Util;

{$R *.lfm}

{ TApplicationForm }

(* Esta rotina é ativada ao clicar no teclado numérico. Ela só tem sentido em algumas fases *)
(* Escolhendo CRED ou DEB; especificando as parcelas; inserindo o valor, inserindo a senha  *)

procedure TApplicationForm.Button1Click(Sender: TObject);
var Key : String;
    ValorTemp : String;
    NumTemp : Longint;
begin
  Key := Copy( (Sender as TComponent).Name, 7, 1);

  if (Fase = FASE_CREDITO_DEBITO) and ((Key = '1') or (Key = '2')) then
  begin
    if (Key = '1') then Taction.SetTransactionType('C');
    if (Key = '2') then Taction.SetTransactionType('D');

    if (Taction.GetTransactionType = 'C') then SetBrand(Taction.Card.GetCardBrand + ' (CRE)')
    else if (Taction.GetTransactionType = 'D') then SetBrand(Taction.Card.GetCardBrand + ' (DEB)');
    Key := '';
    PreencheParcela;
  end;

  if (Fase = FASE_PARCELA) then
  begin
    if (Length(ParcelaEdit.Caption) < 2) then
      ParcelaEdit.Caption := ParcelaEdit.Caption + Key;
  end;

  if (Fase = FASE_VALOR) then
  begin
    DefaultFormatSettings.DecimalSeparator := '.';
    ValorTemp := ValorEdit.Text + Key;
    ValorTemp := StringReplace(ValorTemp, '.', '', [rfReplaceAll]);
    NumTemp := StrtoIntDef(ValorTemp, 0);
    ValorTemp := InttoStr(NumTemp);
    if (Length(ValorTemp) = 1) then ValorTemp := '00' + ValorTemp
    else if (Length(ValorTemp) = 2) then ValorTemp := '0' + ValorTemp;
    ValorTemp := Copy(ValorTemp, 1, Length(ValorTemp) - 2) + '.' + Copy(ValorTemp, Length(ValorTemp) - 1, 2);
    ValorEdit.Text := ValorTemp;
  end;

  if (Fase = FASE_SENHA) then
  begin
    SenhaEdit.Caption := SenhaEdit.Caption + Key;
  end;
end;


(* Botao Cancelar do Cadastro de Cartões. Não confundir com o botão cancelar do POS *)

procedure TApplicationForm.CancelButtonClick(Sender: TObject);
begin
  TabSheet1.TabVisible := TRUE;
  TabSheet2.TabVisible := FALSE;
  SmartGreenPageControl.ActivePage := TabSheet1;
end;


procedure TApplicationForm.CancelClick(Sender: TObject);
begin
  CancelButton1;
end;

(* Botão Aplicar do Cadastro de Cartões. Se tudo OK - validadores - o Cartão é cadastrado *)
(* Notar que se o cartão existir, esta tela atualiza os dados *)

procedure TApplicationForm.ApplyButtonClick(Sender: TObject);
var Card : TCard;
begin
  try
    Card := TCard.Create(Self);
    Card.SetNumber(CardNumberEdit.Text);
    Card.SetCardBrand(BrandCombobox.Items[BrandComboBox.ItemIndex]);
    Card.SetCardHolderName(CardHolderNameEdit.Text);
    Card.SetExpirationDate(ExpirationDateEdit.Text);

    if (TypeRadioGroup.ItemIndex = 0) then
      Card.SetCardType('C')
    else
      Card.SetCardType('T');

    case AcceptRadioGroup.ItemIndex of
      0: Card.SetAccept('C');
      1: Card.SetAccept('D');
      2: Card.SetAccept('CD');
      3: Card.SetAccept('V');
    end;

    Card.SetCurrency(CurrencyComboBox.Items[CurrencyComboBox.ItemIndex]);
    Card.SetHasPassword(HasPassword.Checked);

    if (Card.Validate) then
    begin
      if (Card.CreateCard) then
      begin
        ShowMessage(Card.GetMessage);
        Card.ListCards(CardListView);
        TabSheet1.TabVisible := TRUE;
        TabSheet2.TabVisible := FALSE;
        SmartGreenPageControl.ActivePage := TabSheet1;

      end
      else
      begin
        if (Card.UpdateCard) then
        begin
          ShowMessage(Card.GetMessage);
          Card.ListCards(CardListView);
          TabSheet1.TabVisible := TRUE;
          TabSheet2.TabVisible := FALSE;
          SmartGreenPageControl.ActivePage := TabSheet1;

        end
        else
          ShowMessage(Card.GetMessage);
      end;
    end
    else
      ShowMessage(Card.GetValidateMessage);

  finally
    Card.Query.Close;
    Card.Connection.Close;
    Card.Free;
  end;
end;

(* Tecla <ENTER> do POS. Só tem sentindo em alguns pontos *)
(* Ao digitar o número de parcelas, o valor, a senha *)

procedure TApplicationForm.EnterClick(Sender: TObject);
var NumberTmp : Integer;
    FloatTmp  : Real;
begin
  case Fase of

    FASE_PARCELA :
    begin
      if (Length(ParcelaEdit.Text) <> 0) then
      begin
        NumberTmp := StrtoIntDef(ParcelaEdit.Text, 1);
        if ((NumberTmp > 0) and (NumberTmp < 99)) then
        begin
          Taction.SetNumber(NumberTmp);
          PreencheValor;
        end;
      end;
    end;

    FASE_VALOR :
    begin
       try
         if (Length(ValorEdit.Text) <> 0) then
         begin
           FloatTmp := StrtoFloat(ValorEdit.Text);
           TAction.SetAmount(FloatTmp);
           PreencheSenha;
         end;
       except
       end;
    end;

    FASE_SENHA :
    begin
      if (Length(SenhaEdit.Text) <> 0) then
      begin
        Taction.Card.SetPassword(SenhaEdit.Text);
        Taction.Card.SetPasswordSize(Length(SenhaEdit.Text));
        Enviar;
      end;
    end;
  end;
end;

(* Criação do Form. Neste ponto instancio um TTransaction que será usado em todo o processo *)
(* Importante: dentro do TTransaction tem um TCard, que também é instanciado *)

procedure TApplicationForm.FormCreate(Sender: TObject);
begin
  IsOn := FALSE;
  ApplicationForm.Constraints.MinWidth := 378;
  ApplicationForm.Constraints.MaxWidth := 378;
  ApplicationForm.Width := 378;
  SmartGreenPageControl.ActivePage := TabSheet1;
  Taction := TTransaction.Create(Self);
  Taction.Card := TCard.Create(Self);
end;

(* Destruição das classes para evitar Memory Leak *)

procedure TApplicationForm.FormDestroy(Sender: TObject);
begin
  if (TAction <> nil) then TAction.Card.Free;
  if (TAction <> nil) then TAction.Free;
end;

(* Filtro para caracteres alfabéticos. Usado no cadastro do cartão : HolderName p.ex *)
procedure TApplicationForm.OnlyLetterKeyPress(Sender: TObject; var Key: char);
begin
  Key := UpCase(Key);
  if (Key in ['0'..'9']) then Key := #0;
end;

(* Filtro para caracteres numéricos. Usado no cadastro do cartão : Number p.ex. *)
procedure TApplicationForm.OnlyNumberKeyPress(Sender: TObject; var Key: char);
begin
  if not(Key in ['0'..'9', #9, #8]) then Key := #0;
end;

(* Filtro para caracteres numéricos mais '/'. Usado no cadastro do cartão : Validade p.ex. *)
procedure TApplicationForm.OnlyNumberSlashKeyPress(Sender: TObject;
  var Key: char);
begin
  if not(Key in ['0'..'9', '/', #9, #8]) then Key := #0;
end;

(* Usado pelo boão liga/desliga do POS que fica no canto superior direito *)
procedure TApplicationForm.OnOffButtonChange(Sender: TObject);
begin
  if (IsOn) then PowerOff else PowerOn;
end;

(* Usado na listagem dos caetões cadastrados. Setando o tipo de ListView para ícones pequenos - à la Windows *)
procedure TApplicationForm.ImagesButtonClick(Sender: TObject);
begin
    CardListView.ViewStyle := vsSmallIcon;
end;

(* Evento que ocorre ao trocar de Tab Panel entre Cartões e Transações para sempre buscar as transações salvas no POS *)
procedure TApplicationForm.SmartGreenPageControlChange(Sender: TObject);
begin
  if (SmartGreenPageControl.ActivePage = TabSheet3) then
     Taction.ListTransactions(TransactionListView);
end;

(* Atualizando a tela de transações. Neste ponto está sendo buscado apenas do POS *)
procedure TApplicationForm.UpdateTransactionButtonClick(Sender: TObject);
begin
  TAction.ClearTransactions;
//  Taction.ListTransactions(TransactionListView);
  Taction.List(TransactionListView);
end;

(* Teclado BACKSPACE do POS. Só tem sentido na entrada de dados: parcelas, senha e valor *)
(* Repare que há um tratamento para a entrada de valor. Os números surgem da direita para *)
(* esquerda e o ponto decimal é automático *)

procedure TApplicationForm.BackClick(Sender: TObject);
var ValorTemp : String;
    NumTemp : Longint;
begin
  if (Fase = FASE_VALOR) then
  begin
    ValorTemp := Copy(ValorEdit.Text, 1, Length(ValorEdit.Text) - 1);
    ValorTemp := StringReplace(ValorTemp, '.', '', [rfReplaceAll]);
    NumTemp := StrtoIntDef(ValorTemp, 0);
    ValorTemp := InttoStr(NumTemp);
    if (Length(ValorTemp) = 1) then ValorTemp := '00' + ValorTemp;
    if (Length(ValorTemp) = 2) then ValorTemp := '0' + ValorTemp;
    ValorTemp := Copy(ValorTemp, 1, Length(ValorTemp) - 2) + '.' + Copy(ValorTemp, Length(ValorTemp) - 1, 2);
    ValorEdit.Text := ValorTemp;
  end;
  if (Fase = FASE_PARCELA) then
  begin
    ParcelaEdit.Text := Copy(ParcelaEdit.Text, 1, Length(ParcelaEdit.Text) - 1);
  end;
  if (Fase = FASE_SENHA) then
  begin
    SenhaEdit.Text := Copy(SenhaEdit.Text, 1, Length(SenhaEdit.Text) - 1);
  end;
end;

(* Evento de remoção de um cartão da base local *)
procedure TApplicationForm.BitBtn3Click(Sender: TObject);
var Card : TCard;
begin
  if (CardListView.ItemIndex <> -1) then
  begin
      if (MessageDlg('Importante', 'Deseja remover o cartão selecionado?', mtinformation, [mbYes, mbNo], 0) = mrYes) then
      begin
         Card := TCard.Create(Self);
         Card.SetNumber(CardListView.Items.Item[CardListView.ItemIndex].Caption);
         Card.DeleteCard;
         ShowMessage(Card.GetMessage);
         Card.ListCards(CardListView);
         Card.Free;
      end;
  end;
end;

(* Este evento muda a forma do listview dos cartões para mostrá-lo em forma de relatório *)
procedure TApplicationForm.DetalhesButtonClick(Sender: TObject);
begin
  CardListView.ViewStyle := vsReport;
end;

(* Este botão está na aba de transações e exporta as transações para um Excel. Mesmo em MacOS! *)
procedure TApplicationForm.ExcelButtonClick(Sender: TObject);
var BaseDir : String;
    J : LongInt;
    FName : String;
    MyWorkbook: TsWorkbook;
    MyWorksheet: TsWorksheet;

begin
  try

    Screen.Cursor := crHourGlass;

    BaseDir := GetTempDir(TRUE);

    if (BaseDir = '') then BaseDir := TUtil.CurrentExeDir;

    if (Copy(BaseDir, Length(BaseDir), 1) <> DirectorySeparator) then
      BaseDir := BaseDir + DirectorySeparator;

    FName := 'tmp' + FormatDateTime('MMDDHHNNSS', Now) + '.xlsx';

    MyWorkbook := TsWorkbook.Create;
    MyWorksheet := MyWorkbook.AddWorksheet('Transacoes');

    MyWorksheet.WriteText(0, 0, 'Número');
    MyWorksheet.WriteText(0, 1, 'Valor');
    MyWorksheet.WriteText(0, 2, 'Moeda');
    MyWorksheet.WriteText(0, 3, 'Tipo');
    MyWorksheet.WriteText(0, 4, 'Parcelas');
    MyWorksheet.WriteText(0, 5, 'Status');
    MyWorksheet.WriteText(0, 6, 'Resposta');
    MyWorksheet.WriteText(0, 7, 'Autorização');
    MyWorksheet.WriteText(0, 8, 'Timestamp');

    for J := 1 to TransactionListView.Items.Count do
    begin
      MyWorksheet.WriteText(J, 0, TransactionListView.Items[J-1].Caption);
      MyWorksheet.WriteText(J, 1, TransactionListView.Items[J-1].SubItems[0]);
      MyWorksheet.WriteText(J, 2, TransactionListView.Items[J-1].SubItems[1]);
      MyWorksheet.WriteText(J, 3, TransactionListView.Items[J-1].SubItems[2]);
      MyWorksheet.WriteText(J, 4, TransactionListView.Items[J-1].SubItems[3]);
      MyWorksheet.WriteText(J, 5, TransactionListView.Items[J-1].SubItems[4]);
      MyWorksheet.WriteText(J, 6, TransactionListView.Items[J-1].SubItems[5]);
      MyWorksheet.WriteText(J, 7, TransactionListView.Items[J-1].SubItems[6]);
      MyWorksheet.WriteText(J, 8, TransactionListView.Items[J-1].SubItems[7]);
    end;

    MyWorkbook.WriteToFile(BaseDir + FName, sfOOXML);
    MyWorkbook.Free;

    if (FileExists(BaseDir + FName) { *Converted from FileExists* }) then
       OpenDocument(PChar(BaseDir + FName)); { *Converted from ShellExecute* }

  finally
    Screen.Cursor := crDefault;
  end;
end;

(* Evento de duplo clique na Lista de Cartões. Ao realizar isso o sistema muda para *)
(* a aba de detalhes para editar uma informação de um cartão *)

procedure TApplicationForm.CardListViewDblClick(Sender: TObject);
var Card : TCard;
    Item : Longint;
begin
  if (CardListView.ItemIndex <> -1) then
  begin
    Card := TCard.Create(Self);
    Card.SetNumber(CardListView.Items.Item[CardListView.ItemIndex].Caption);
    Card.ReadCard;

    CardNumberEdit.Text := Card.GetNumber;

    Item := BrandComboBox.Items.IndexOf(Card.GetCardBrand);
    if (Item = -1) then Item := 0;
    BrandCombobox.ItemIndex := Item;

    CardHolderNameEdit.Text := Card.GetCardHolderName;
    ExpirationDateEdit.Text := Card.GetExpirationDate;

    if (Card.GetCardType = 'C') then
      TypeRadioGroup.ItemIndex := 0
    else
      TypeRadioGroup.ItemIndex := 1;

    if Card.GetAccept = 'C' then AcceptRadioGroup.ItemIndex := 0;
    if Card.GetAccept = 'D' then AcceptRadioGroup.ItemIndex := 1;
    if Card.GetAccept = 'CD' then AcceptRadioGroup.ItemIndex := 2;
    if Card.GetAccept = 'V' then AcceptRadioGroup.ItemIndex := 3;

    Item := CurrencyComboBox.Items.IndexOf(Card.GetCurrency);
    if (Item = -1) then Item := 0;
    CurrencyComboBox.ItemIndex := Item;

    HasPassword.Checked := Card.GetHasPassword;

    Card.Free;
    TabSheet2.TabVisible := TRUE;
    {$IFDEF WINDOWS}
    TabSheet1.TabVisible := FALSE;
    {$ENDIF}
    SmartGreenPageControl.ActivePage := TabSheet2;
  end;
end;

(* Um ponto bem legal do sistema. Ao ir digitando o número do cartão o sistema tenta *)
(* descobrir a bandeira através de uma base de dados pré-existente *)

 procedure TApplicationForm.CardNumberEditKeyUp(Sender: TObject);
var CardList : TCardList;
    Brand : String;

begin
  // Rotina para descobrir o cartão e preencher os campos de acordo

  Brand := TCard.GetBrandByCardNumber(Trim(CardNumberEdit.Text));
  CardList := TCard.GetDefaultAttributesByCardBrand(Brand);

  if (CardList.CardBrand <> '') then
  begin
    // Encontrou valores padroes
    BrandComboBox.ItemIndex := BrandComboBox.Items.IndexOf(CardList.CardBrand);

    if (CardList.CardType = 'C') then
      TypeRadioGroup.ItemIndex := 0
    else
      TypeRadioGroup.ItemIndex := 1;

    if (CardList.Accept = 'C') then  AcceptRadioGroup.ItemIndex := 0;
    if (CardList.Accept = 'D') then  AcceptRadioGroup.ItemIndex := 1;
    if (CardList.Accept = 'CD') then  AcceptRadioGroup.ItemIndex := 2;
    if (CardList.Accept = 'V') then  AcceptRadioGroup.ItemIndex := 3;

    HasPassword.Checked := CardList.HasPassword;
  end
  else
  begin
    // Nao e uma bandeira aceita. Mantem a bandeira mas o restante fica o valor default
    BrandComboBox.ItemIndex := BrandComboBox.Items.IndexOf(Brand);
    TypeRadioGroup.ItemIndex := 0;
    AcceptRadioGroup.ItemIndex := 0;
    HasPassword.Checked := TRUE;
  end;
end;

 (* Evento do botão (+) para ir para a aba de cadastro de cartões *)
procedure TApplicationForm.CriarButtonClick(Sender: TObject);
var Card : TCard;
begin
  // Zera campos
  ZeraCamposCard;

  // Habilita aba de cartões e desabilita a de CRUD
  TabSheet2.TabVisible := TRUE;
  TabSheet1.TabVisible := FALSE;
  SmartGreenPageControl.ActivePage := TabSheet2;
  CardNumberEdit.SetFocus;

  // Lista Cartoes
  Card := TCard.Create(Self);
  Card.ListCards(CardListView);
  Card.Free;
end;

(* Uma about form do sistema *)
procedure TApplicationForm.AjudaClick(Sender: TObject);
begin
  AboutForm := TAboutForm.Create(Self);
  AboutForm.ShowModal;
  AboutForm.Free;
end;

(* ao teclar no botão <atalhos> chama a URL de Dev da Stone. Só para habilitar algo para o botão *)
procedure TApplicationForm.AtalhosClick(Sender: TObject);
begin
  TUtil.AbrirPagina('https://devcenter.stone.com.br/');
end;

(* Evento a ser chamado para limpar os campos da tela e esconder todos os campos *)
(* de ediçao da tela do POS. Volta para o ponto de inserção de cartão *)
procedure TApplicationForm.DesligaCamposTela;
begin
  ValorEdit.Visible := FALSE;
  ValorLabel.Visible := FALSE;
  SenhaEdit.Visible := FALSE;
  SenhaLabel.Visible := FALSE;
  ParcelaEdit.Visible:= FALSE;
  ParcelaLabel.Visible := FALSE;
  CreditoDebitoLabel.Visible := FALSE;
  ValorEdit.Caption := '';
  SenhaEdit.Caption := '';
  ParcelaEdit.Caption := '';
  SetBrand('');
end;

(* Zera os campos da tela de cadastro de cartões *)
procedure TApplicationForm.ZeraCamposCard;
begin
    // Zera campos
  CardNumberEdit.Text := '';
  BrandCombobox.ItemIndex := 0;
  CardHolderNameEdit.Text := '';
  ExpirationDateEdit.Text := '';
  TypeRadioGroup.ItemIndex := 0;
  AcceptRadioGroup.ItemIndex := 0;
  CurrencyComboBox.ItemIndex := 0;
  HasPassword.Checked := TRUE;
end;

(* Além de habilitar a tela, seta um texto tal como - Conectando... *)
procedure TApplicationForm.SetTextoTela(Msg : String);
begin
  Tela.Visible := TRUE;
  Tela.Caption := Msg;
end;

(* Esta opção desliga a tela completamente. Volta para a inserção de cartão *)
procedure TApplicationForm.TelaOff;
begin
  Tela.Caption := '';
  Tela.Visible := FALSE;
end;

(* Liga o Pos. Aqui há um este no Taction.start para saber se a POS está habilitada pela stone *)
(* O teste é feito enviando o serial e o stone code para o servidor *)
procedure TApplicationForm.PowerOn;
var OK : Boolean;
    Card : TCard;
begin
  // Ligando o equipamento. Há uma verificação para saber se a máquina está habilitada
  OnOffButton.Visible := FALSE;
  Tela.Caption := 'Iniciando...';
  Application.ProcessMessages;
//  Sleep(500);
  TelaTopo.Visible := FALSE;

  IsOn := TRUE;

  OK := Taction.Start;
  if (OK) then
  begin
    Fase := FASE_AGUARDANDO;
    TelaOff;
    SetMerchant(Taction.GetMerchant);

    Card := TCard.Create(Self);
    Card.ListCards(CardListView);
    Card.Free;

    SmartGreenPageControl.Visible := TRUE;
    ApplicationForm.Constraints.MinWidth := 700;
    ApplicationForm.Constraints.MaxWidth := 700;
    ApplicationForm.Width := 700;
  end
  else
  begin
    Tela.Caption := Taction.GetMessageCode+#13#10+Taction.GetMessage;
    SmartGreenPageControl.Visible := FALSE;
  end;
  OnOffButton.Caption := 'OFF';
  OnOffButton.Visible := TRUE;
end;

(* Desliga o Pos, zerando todas as informações e escondendo a tela de cadastro e de transações *)
procedure TApplicationForm.PowerOff;
begin
  // Desligando o equipamento. Apenas zerando os dados e desligando a tela.
  OnOffButton.Visible := FALSE;
  SetTextoTela('Desligando...');
  Application.ProcessMessages;
  Sleep(500);
  DesligaCamposTela;
  SetTextoTela('');
  SetMerchant('');
  SetBrand('');
  ApplicationForm.Constraints.MinWidth := 378;
  ApplicationForm.Constraints.MaxWidth := 378;
  ApplicationForm.Width := 378;
  SmartGreenPageControl.Visible := FALSE;

  TelaTopo.Visible := TRUE;
  IsOn := FALSE;
  OnOffButton.Caption := 'ON';
  OnOffButton.Visible := TRUE;
end;

(* Evento utilizado para cancelar uma transação. Repare que se estiver enviando *)
(* a transação não pode mais ser cancelada *)

procedure TApplicationForm.CancelButton1;
begin
  if (Fase <> FASE_ENVIANDO) then
  begin
      // Cancelando todas as operações.
    DesligaCamposTela;
    InserirButton.Caption := 'Inserir Cartão';
    SetTextoTela('Cancelando...');
    Application.ProcessMessages;
    Sleep(500);
    SetBrand('');
    SmartGreenPageControl.Enabled := TRUE;
    TelaOff;
    ValorEdit.Caption := '';
    ParcelaEdit.Caption := '';
    SenhaEdit.Caption := '';
  end;
end;

(* Primeira fase do processo de inserção de dados se o label for "Inserir Cartão". *)
(* Se o cartão for crédito/débito o sistema apresentará as opções e aguardará o próximo passo *)
(* Caso contrário ele cai para o Preenchimento de Parcelas *)
(* Também pode ser a última fase do processo caso o label do botão seja Retirar Cartão *)
procedure TApplicationForm.InserirButtonClick(Sender: TObject);
begin
  if (InserirButton.Caption = 'Retirar Cartão') then
  begin
    DesligaCamposTela;
    TelaOff;
    InserirButton.Caption := 'Inserir Cartão';
  end
  else
  begin
    if (CardListView.ItemIndex <> -1) then
    begin
      try
        SmartGreenPageControl.Enabled := FALSE;
        InserirButton.Caption := 'Retirar Cartão';
        DesligaCamposTela;

        Taction.Card.SetNumber(CardListView.Items.Item[CardListView.ItemIndex].Caption);
        if (Taction.Card.ReadCard) then
        begin
          Fase := FASE_CREDITO_DEBITO;
          SetTextoTela('');
          SetBrand(Taction.Card.GetCardBrand);
          Application.ProcessMessages;

          // setBrand identifica se a compra é no Crédito/Débito ou Voucher. Na Transação VCH vira DEB
          if (Taction.Card.GetAccept = 'CD') then
            CreditoDebitoOn
          else
          begin
            if (Taction.Card.GetAccept = 'C') then SetBrand(Taction.Card.GetCardBrand + ' (CRE)')
            else if (Taction.Card.GetAccept = 'D') then SetBrand(Taction.Card.GetCardBrand + ' (DEB)')
            else if (Taction.Card.GetAccept = 'V') then SetBrand(Taction.Card.GetCardBrand + ' (VCH)');
            Taction.SetTransactionType(Taction.Card.GetAccept);
            PreencheParcela;
          end;
        end;
      except
        on E: Exception do
        begin
         DesligaCamposTela;
         SetTextoTela('Erro ao processar o cartão' + #13#10 + E.Message + #13#10 + 'Retire o cartão');
        end
      end;
    end;
  end;
end;

(* Passo de preenchimento de parcelas. O sistema só para aqui se o cartão for de *)
(* crédito ou se for um combo mas foi escolhido crédito. Caso contrário, pula para *)
(* o preenchimento de valor *)

procedure TApplicationForm.PreencheParcela;
begin
  try
    Fase := FASE_PARCELA;
    CreditoDebitoOff;

    if (Taction.GetTransactionType = 'C') then
    begin
      ParcelaEdit.Text := '';
      ParcelaOn;
      ParcelaEdit.SetFocus;
    end
    else
      PreencheValor;

  except
    on E: Exception do
    begin
     DesligaCamposTela;
     SetTextoTela('Erro ao processar o cartão' + #13#10 + E.Message + #13#10 + 'Retire o cartão');
    end
  end;
end;

(* Preenchimento de valor. Aqui o sistema sempre para pois é um dado obrigatório *)
(* Após isso vai para o passo de senha *)

procedure TApplicationForm.PreencheValor;
begin
  try
    Fase := FASE_VALOR;
    CreditoDebitoOff;
    ValorEdit.Text := '';
    ValorOn;
    ValorEdit.SetFocus;
  except
    on E: Exception do
    begin
     DesligaCamposTela;
     SetTextoTela('Erro ao processar o cartão' + #13#10 + E.Message + #13#10 + 'Retire o cartão');
    end
  end;
end;

(* Se o cartão possui senha para aqui para o preenchimento. Caso contrário vai para o envio da *)
(* transação *)
procedure TApplicationForm.PreencheSenha;
begin
  try
    Fase := FASE_SENHA;
    if (Taction.Card.GetHasPassword) then
    begin
      SenhaEdit.Text := '';
      SenhaOn;
      SenhaEdit.SetFocus;
    end
    else
      Enviar;

  except
    on E: Exception do
    begin
     DesligaCamposTela;
     SetTextoTela('Erro ao processar o cartão' + #13#10 + E.Message + #13#10 + 'Retire o cartão');
    end
  end;
end;

(* Envio transacao no Taction.Send. Lá mesmo, a transação é salva ao retornar com o *)
(* Código de autorização. Caso contrário um erro é recebido *)
(* Depois disso fica aguardando o usuário retira o cartão *)

procedure TApplicationForm.Enviar;
begin
  try try
    Fase := FASE_ENVIANDO;

    SenhaOff;
    ValorOff;
    ParcelaOff;
    SetTextoTela('Conectando...');
    Taction.Send;
    SetTextoTela(TAction.GetMessageCode + #13#10 + TAction.GetMessage + #13#10#13#10+ 'Retire o cartão');
    Fase := FASE_FINAL;
  except
    on E: Exception do
    begin
     DesligaCamposTela;
     SetTextoTela('Erro ao processar o cartão' + #13#10 + E.Message + #13#10 + 'Retire o cartão');
     Fase := FASE_FINAL;
    end
  end;
  finally
    SmartGreenPageControl.Enabled := TRUE;
  end;
end;

(* Seta um texto para a linha de bandeira do POS. Parte superior *)
procedure TApplicationForm.SetBrand(Brand : String);
begin
  BrandLabel.Caption := Brand;
end;

(* Sera um texto para a linha do estabelecimento do POS. Parte inferior *)
procedure TApplicationForm.SetMerchant(Merchant : String);
begin
  MerchantPanel.Caption := Merchant;
end;

(* Habilita o texto 1-Crédito ; 2-Débito na tela do POS *)
(* Usado se for um cartão combo *)

procedure TApplicationForm.CreditoDebitoOn;
begin
  CreditoDebitoLabel.Visible := TRUE;
  Application.ProcessMessages;
end;

(* Desliga o texto 1-Crédito ; 2-Débito na tela do POS *)
procedure TApplicationForm.CreditoDebitoOff;
begin
  CreditoDebitoLabel.Visible := FALSE;
end;

(* Habilita a linha de preenchimento de valor *)
procedure TApplicationForm.ValorOn;
begin
  ValorLabel.Visible := TRUE;
  ValorEdit.Visible := TRUE;
end;

(* Desabilita a linha de preenchimento de valor *)
procedure TApplicationForm.ValorOff;
begin
  ValorLabel.Visible := FALSE;
  ValorEdit.Visible := FALSE;
end;

(* Habilita a linha de preenchimento de parcelas *)
procedure TApplicationForm.ParcelaOn;
begin
  ParcelaLabel.Visible := TRUE;
  ParcelaEdit.Visible := TRUE;
end;

(* Desabilita a linha de preenchimento de parcelas *)
procedure TApplicationForm.ParcelaOff;
begin
  ParcelaLabel.Visible := FALSE;
  ParcelaEdit.Visible := FALSE;
end;

(* Habilita a linha de preenchimento de senha *)
procedure TApplicationForm.SenhaOn;
begin
  SenhaLabel.Visible := TRUE;
  SenhaEdit.Visible := TRUE;
end;

(* Desabilita a linha de preenchimento de senha *)
procedure TApplicationForm.SenhaOff;
begin
  SenhaLabel.Visible := FALSE;
  SenhaEdit.Visible := FALSE;
end;

end.

