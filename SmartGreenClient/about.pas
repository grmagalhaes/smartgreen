unit about;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Buttons;

type

  { TAboutForm. Apenas um about box ao clicar no botão ajuda do pos }

  TAboutForm = class(TForm)
    Image2: TImage;
    OKButton: TBitBtn;
    Image1: TImage;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    procedure OKButtonClick(Sender: TObject);
  private

  public

  end;

var
  AboutForm: TAboutForm;

implementation

{$R *.lfm}

{ TAboutForm }

procedure TAboutForm.OKButtonClick(Sender: TObject);
begin
  Close;
end;

end.

