unit transaction;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, IBConnection, sqldb,fpjson, jsonparser, Card, ComCtrls;

type

  { TTransaction. Classe responsável por configurar, enviar para o servidor e armazenar uma transação.}
  { Também por aqui é possível listar as transações localmente }

  TTransaction = class(TDataModule)
    Connection: TIBConnection;
    Query: TSQLQuery;
    SQLTransaction: TSQLTransaction;
    Card : TCard;
    procedure DataModuleCreate(Sender: TObject);
  private
    Amount : Real;
    TransactionType : String;
    Number : Integer;
    Merchant : String;
    Currency : String;
    Status : String;
    Authorization : String;
    Timestamp : TDateTime;

    StoneCode  : String;
    Serial     : String;
    MerchantID : String;
    URL        : String;

    MessageCode: String;

    function CreateJSON : String;
    function HTTPSend(Service: String; HTTPRequest : String; var HTTPStatus : String; var HTTPResponse : String) : Boolean;

  public
    function GetMessage : String;

    function GetMessageCode : String;
    procedure SetMessageCode(S : String);


    function Start : Boolean;
    function Send : Boolean;

    procedure SetMerchant(S : String);
    function GetMerchant : String;

    procedure SetAmount(R : Real);
    function GetAmount : Real;

    procedure SetCurrency(S : String = 'BRL');
    function GetCurrency : String;

    procedure SetTransactionType(S : String);
    function GetTransactionType : String;

    procedure SetNumber(N : Integer);
    function GetNumber : Integer;

    procedure SetAuthorization(S : String);
    function GetAuthorization : String;

    procedure SetStatus(S : String);
    function GetStatus : String;

    function CreateTransaction : Boolean;
    procedure ListTransactions(TransactionListView : TListView);
    function ClearTransactions : Boolean;
    procedure List(TransactionListView : TListView);

end;

implementation

{$R *.lfm}

uses IniFiles, Util, fphttpclient;

// Retorna a última mensagem de um comando
function TTransaction.GetMessage : String;
var IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'chip.ini');
  Result := IniFile.ReadString('MESSAGE', MessageCode, '8000');
  IniFile.Free;
end;

// retorna o código da última mensagem retornada do servidor
function TTransaction.GetMessageCode : String;
begin
  Result := MessageCode;
end;

// seta o código da última mensagem retornada do servidor
procedure TTransaction.SetMessageCode(S : String);
begin
  MessageCode := S;
end;

// inicialização do módulo com valores default. o arquivo chip.ini tem informações para sobreecrever estes
procedure TTransaction.DataModuleCreate(Sender: TObject);
begin
  Connection.DatabaseName := TUtil.CurrentExeDir() + DirectorySeparator + 'smart_green.fdb';
  MessageCode := '8000';
  StoneCode := '0';
  Serial := '0';
  MerchantID := '0';
end;

// cria um JSON de uma transação para ser enviada ao servidor
function TTransaction.CreateJSON : String;
var JSONText : String;
begin

  DefaultFormatSettings.ShortDateFormat := 'dd/mm/yyyy';

  DefaultFormatSettings.ShortDateFormat := 'dd/mm/yyyy';
  DefaultFormatSettings.ShortDateFormat := 'dd/mm/yyyy';
  DefaultFormatSettings.DecimalSeparator := '.';
  DefaultFormatSettings.ThousandSeparator := ',';

  JSONText :=               '{"transaction":';
  JSONText := JSONText + '{';

  JSONText := JSONText +   '"card":';
  JSONText := JSONText +     '{';
  JSONText := JSONText +       '"number":"' + Self.Card.GetNumber + '",';
  JSONText := JSONText +       '"card_holder_name":"' + Self.Card.GetCardHolderName + '",';
  JSONText := JSONText +       '"brand":"' + Self.Card.GetCardBrand + '",';
  JSONText := JSONText +       '"expiration_date":"' + Self.Card.GetExpirationDate + '",';
  JSONText := JSONText +       '"type":"' + Self.Card.GetCardType + '",';
  JSONText := JSONText +       '"accept":"' + Self.Card.GetAccept + '",';
  JSONText := JSONText +       '"has_password":"' + BooltoStr(Self.Card.GetHasPassword, true) + '",';
  JSONText := JSONText +       '"currency":"' + Self.Card.GetCurrency + '",';
  JSONText := JSONText +       '"password":"' + Self.Card.GetPassword + '",';
  JSONText := JSONText +       '"password_size":' + InttoStr(Self.Card.GetPasswordSize) + ',';
  JSONText := JSONText +       '"accepted_by_stone":"' + BooltoStr(Self.Card.IsAccepted(Self.Card.GetCardBrand), true) + '"';
  JSONText := JSONText +     '},';

  JSONText := JSONText +   '"amount":' + StringReplace(FloattoStrF(Amount, ffnumber, 10, 2), ',', '', [rfReplaceAll]) + ',';
  JSONText := JSONText +   '"currency":"' + Currency + '",';
  JSONText := JSONText +   '"type":"' + TransactionType + '",';
  JSONText := JSONText +   '"number_installments":' + InttoStr(Number) + ',';
  JSONText := JSONText +   '"transaction_ts":"' + DateTimetoStr(Timestamp) + '",';
  JSONText := JSONText +   '"merchant":"' + MerchantID + '",';
  JSONText := JSONText +   '"stonecode":"' + StoneCode + '",';
  JSONText := JSONText +   '"serial":"' + Serial + '"';
  JSONText := JSONText + '}';
  JSONText := JSONText + '}';

  Result := JSONText;

end;

// envia a requisição para o REST Server (Smart Green Server). O status e a resposta retornadas são retornados para gravar a transação
function TTransaction.HTTPSend(Service: String; HTTPRequest : String; var HTTPStatus : String; var HTTPResponse : String) : Boolean;
Var
  httpClient : TFPHTTPClient;
begin
  HTTPResponse := '';
  HTTPStatus := '';
  httpClient := TFPHTTPClient.Create(nil);
  httpClient.AddHeader('Accept','application/json');
  httpClient.AddHeader('Content-Type','application/json');

  httpClient.RequestBody := TStringStream.Create(HTTPRequest);
  try
    HTTPResponse := httpClient.Post(Service);
    HTTPStatus := httpClient.ResponseStatusText;

    Result := TRUE;
  except
    on e: Exception do
    begin
      HTTPStatus := httpClient.ResponseStatusText;
      Result := FALSE;
      httpClient.Free;
      Exit;
    end;
  end;
  httpClient.Free;
end;

// inicia uma transação. O primeiro passo é verificar se o equipamento está habilitado
function TTransaction.Start : Boolean;
var IniFile : TIniFile;
    HTTPResponse : String;
    HTTPStatus : String;
    JSONText : String;
    Ret : Boolean;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'chip.ini');
  HTTPStatus := '';
  HTTPResponse := '';
  Serial := IniFile.ReadString('POS', 'SERIAL', '00000000');
  StoneCode := IniFile.ReadString('POS', 'STONECODE', '00000000');
  SetMerchant(IniFile.ReadString('MERCHANT', 'NOME', 'Noname'));
  MerchantID := IniFile.ReadString('MERCHANT', 'ID', '00000000');
  URL := IniFile.ReadString('SERVICE', 'URL', 'http://localhost:8080/sgs');
  Currency := IniFile.ReadString('MERCHANT', 'CURRENCY', 'BRL');

  JSONText :=            '{"transaction":';
  JSONText := JSONText +   '{';
  JSONText := JSONText +     '"merchant":"' + MerchantID + '",';
  JSONText := JSONText +     '"stonecode":"' + StoneCode + '",';
  JSONText := JSONText +     '"serial":"' + Serial + '"';
  JSONText := JSONText +   '}';
  JSONText := JSONText + '}';

  Ret := HTTPSend(URL + '/start', JSONText, HTTPStatus, HTTPResponse);

  if Ret then
  begin
    if (Pos('OK', HTTPResponse) <> 0) then
    begin
      MessageCode := '0000';  // Equipamento habilitado
      Result := TRUE;
    end
    else
    begin
      MessageCode := '1009';    // Equipamento com problema
      Result := FALSE;
    end;
  end
  else
  begin
    MessageCode := '9108';    // Erro de transmissão
    Result := FALSE;
  end;

  IniFile.Free;
end;

// processo de envio de uma transacao.
function TTransaction.Send : Boolean;
var
  HTTPRequest : String;
  HTTPStatus : String;
  HTTPResponse : String;
  Ret : Boolean;
  jData : TJSONData;
  jObject : TJSONObject;
  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'chip.ini');
  URL := IniFile.ReadString('SERVICE', 'URL', 'http://localhost:8080/sgs');
  IniFile.Free;

  HTTPRequest := CreateJSON;
  HTTPStatus := '';
  HTTPResponse := '';

  Ret := HTTPSend(URL + '/transaction', HTTPRequest, HTTPStatus, HTTPResponse);

  if Ret then
  begin
    jData := GetJSON(HTTPResponse);
    if (jData <> nil) then
    begin
      jObject := TJSONObject(jData);
      SetAuthorization(jObject.Get('authorization'));
      SetStatus(jObject.Get('status'));
      SetMessageCode(jObject.Get('code'));

      if (Ret) then
        CreateTransaction();
    end
    else
    begin
      SetStatus('4');
      SetMessageCode('9109');
    end;
  end;
  if (Status = '3') then Result := TRUE else Result := FALSE;

end;

procedure TTransaction.List(TransactionListView : TListView);
var ItemView : TListItem;
  HTTPRequest : String;
  HTTPStatus : String;
  HTTPResponse : String;
  Ret : Boolean;
  jData : TJSONData;
  jArray :TJSONArray;
  i : Integer;
  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(TUtil.CurrentExeDir + DirectorySeparator + 'chip.ini');
  MerchantID := IniFile.ReadString('MERCHANT', 'ID', '00000000');
  IniFile.Free;

  HTTPRequest := '{"id":"' + MerchantID + '"}';
  HTTPStatus := '';
  HTTPResponse := '';

  Ret := HTTPSend(URL + '/list', HTTPRequest, HTTPStatus, HTTPResponse);

  if Ret then
  begin
    jData := GetJSON(HTTPResponse);
    if (jData <> nil) then
    begin
      jArray := TJSONArray(jData);

      TransactionListView.BeginUpdate;
      TransactionListView.Items.Clear;
      for i := 0 to jArray.Count - 1 do
      begin
        ItemView := TransactionListView.Items.Add;
        ItemView.Caption := jArray.Items[i].FindPath('card_number').AsString;
        ItemView.SubItems.Add(FloattoStrF(jArray.Items[i].FindPath('amount').AsFloat, ffNumber, 10,2));
        ItemView.SubItems.Add(jArray.Items[i].FindPath('currency').AsString);
        ItemView.SubItems.Add(jArray.Items[i].FindPath('transaction_type').AsString);
        ItemView.SubItems.Add(InttoStr(jArray.Items[i].FindPath('number_installments').AsInteger));
        ItemView.SubItems.Add(jArray.Items[i].FindPath('status').AsString);
        ItemView.SubItems.Add(jArray.Items[i].FindPath('response').AsString);
        ItemView.SubItems.Add(jArray.Items[i].FindPath('authorization').AsString);
        ItemView.SubItems.Add(jArray.Items[i].FindPath('transaction_ts').AsString);
      end;
      TransactionListView.EndUpdate;
    end
    else
    begin
      SetStatus('4');
      SetMessageCode('9109');
    end;
  end;
end;


// getter e setter da classe
// merchant = estabelecimento
procedure TTransaction.SetMerchant(S : String);
begin
  Merchant := S;
end;

function TTransaction.GetMerchant : String;
begin
  Result := Merchant;
end;

// valor
procedure TTransaction.SetAmount(R : Real);
begin
  Amount := R
end;

function TTransaction.GetAmount : Real;
begin
  Result := Amount;
end;

// moeda. lembrando que se a moeda do cartão for diferente do cartão
// uma conversão deverá ser feita
procedure TTransaction.SetCurrency(S : String = 'BRL');
begin
  Currency := S;
end;

function TTransaction.GetCurrency : String;
begin
  Result := Currency;
end;

// crédito ou débito. Voucher vira débito na transação
procedure TTransaction.SetTransactionType(S : String);
begin
  TransactionType := UpperCase(Copy(S, 1, 1));
  if (TransactionType = 'V') then TransactionType := 'D';
end;

function TTransaction.GetTransactionType : String;
begin
  Result := TransactionType;
end;

// seta o número de parcelas.
procedure TTransaction.SetNumber(N : Integer);
begin
  Number := N;
end;

function TTransaction.GetNumber : Integer;
begin
  Result := Number;
end;

// seta o código de autorização retornado pelo servidor
procedure TTransaction.SetAuthorization(S : String);
begin
  Authorization := S;
end;

function TTransaction.GetAuthorization : String;
begin
  Result := Authorization;
end;

// seta o código de autorização retornado pelo servidor
procedure TTransaction.SetStatus(S : String);
begin
  Status := S;
end;

function TTransaction.GetStatus : String;
begin
  Result := Status;
end;

// armazena a transação localmente
function TTransaction.CreateTransaction : Boolean;
begin
  try try
    Connection.Open;
    Query.SQL.Clear;
    Query.SQL.Add('insert into transaction_ (card_number, amount, currency, transaction_type, number_installments, merchant_id, status, response, ');
    Query.SQL.Add('authorization, password_, password_size, transaction_ts) ');
    Query.SQL.Add('values ');
    Query.SQL.Add('(:card_number, :amount, :currency, :transaction_type, :number_installments, :merchant_id, :status, :response,');
    Query.SQl.Add(' :authorization, :password_, :password_size, :transaction_ts)');
    Query.ParamByName('card_number').AsString := Self.Card.GetNumber;
    Query.ParamByName('amount').AsFloat := Amount;
    Query.ParamByName('currency').AsString := Currency;
    Query.ParamByName('transaction_type').AsString := TransactionType;
    Query.ParamByName('number_installments').AsInteger := Number;
    Query.ParamByName('merchant_id').AsString := MerchantID;
    Query.ParamByName('status').AsString := Status;
    Query.ParamByName('response').AsString := MessageCode;
    Query.ParamByName('authorization').AsString := Authorization;
    Query.ParamByName('password_').AsString := Self.Card.GetPassword;
    Query.ParamByName('password_size').AsInteger := Self.Card.GetPasswordSize;
    TimeStamp := Now;
    Query.ParamByName('transaction_ts').AsDateTime := TimeStamp;

    Query.ExecSQL;
    Result := TRUE;
  except
    on E: Exception do
    begin
      Result := FALSE;
      MessageCode := E.Message;
    end;
  end;
  finally
    Query.Close;
    Connection.Close;
  end;
end;


// lista todas as transações armazenadas localmente
procedure TTransaction.ListTransactions(TransactionListView : TListView);
var ItemView : TListItem;
begin
  Query.SQL.Clear;
  Query.SQL.Add('select * from transaction_ order by transaction_ts desc');
  Query.Open;
  TransactionListView.Items.Clear;
  while (not Query.Eof) do
  begin
    ItemView := TransactionListView.Items.Add;
    ItemView.Caption := Query.FieldByName('card_number').AsString;
    ItemView.SubItems.Add(FloattoStrF(Query.FieldByName('amount').AsFloat, ffNumber, 10,2));
    ItemView.SubItems.Add(Query.FieldByName('currency').AsString);
    ItemView.SubItems.Add(Query.FieldByName('transaction_type').AsString);
    ItemView.SubItems.Add(Query.FieldByName('number_installments').AsString);
    ItemView.SubItems.Add(Query.FieldByName('status').AsString);
    ItemView.SubItems.Add(Query.FieldByName('response').AsString);
    ItemView.SubItems.Add(Query.FieldByName('authorization').AsString);
    ItemView.SubItems.Add(Query.FieldByName('transaction_ts').AsString);
    Query.Next;
  end;
  Query.Close;
  Connection.Close;
end;

// limpa todas as transações locais
function TTransaction.ClearTransactions : Boolean;
begin
  try try
    Connection.Open;
    Query.SQL.Clear;
    Query.SQL.Add('delete from transaction_');
    Query.ExecSQL;
    Result := TRUE;
    MessageCode := 'Transações apagadas';
  except
    on E: Exception do
    begin
      Result := FALSE;
      MessageCode := E.Message;
    end;
  end;
  finally
    Query.Close;
    Connection.Close;
  end;
end;

end.

