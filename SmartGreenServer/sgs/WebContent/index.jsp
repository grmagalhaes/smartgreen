<!DOCTYPE html>
<%@ page session="false" %>

<%
response.setHeader("X-XSS-Protection","1; mode=block");
response.setHeader("Content-Language", "pt-BR");

%>
<html>
<head>
<title>:Smart Green Server::</title>
<link rel="stylesheet" href="sms.css">

<meta name="viewport" content="width=1024"> 

<meta name="author" content="Gerson Rodrigues"> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 


<link href="stone.ico" rel="SHORTCUT ICON">  
<link href="stone.ico" rel="ICON">    
<link rel="stylesheet" href="/sgs/sgs.css">  
<script type="text/javascript" src="/sgs/sgs.js"></script>  

</head>
<body class="bodyclass">
<div id="tudo"> 
  <div id="topo">
	 <table class="tabtopo"> 
	  <tr>
	  <td><a style="border:none" href="https://stone.com.nr"><img height="100px" width="auto" alt="Smart Green Server" style="border:none" src="images/stone.png"></a></td>
	  <td style="float:right;position:relative;top:3px;width:5px">&nbsp;</td>
	  <td style="float:right;width:15px;">
	  </tr>
	</table> 
	<table style="border-width:0px;!important" class="tabtopstatus">
	    <tr><td><table class="tabbarra"> 
 	    <tr><td style="float:left;position:relative;margin-left:2px">
 	    <div style="float:left" id="barra2" class="barra"></div>	
 	    <div style="float:left" id="barra3" class="barra"></div> 
 	    <div style="float:left" id="barra4" class="barra"></div>
 	    <div style="float:left" id="barra5" class="barra"></div> 	    
 	    <div style="float:left" id="barra6" class="barra"></div> 	    
 	    <div style="float:left" id="barra7" class="barra"></div>
   	    <div style="float:left" id="barra8" class="barra"></div>
 	    <div style="float:left" id="barra9" class="barra"></div>   	    
 	    <div style="float:left" id="barra10" class="barra"></div>
 	    <div style="float:left" id="barra11" class="barra"></div>
 	    <div style="float:left" id="barra12" class="barra"></div>
 	    </td></tr>
 	  </table>
 	  </td></tr>
	</table>  
  </div>
</div>
</body>
</html>

 