var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6

try {
  xmlhttp = new XMLHttpRequest();
} catch(ee) {
  try {
    xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
  } catch(e) {
    try {
        xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
    } catch(E) {
      xmlhttp = false;
    }
  }
}

var max_barras = 12;

function carrega(numero, titulo, elemento, endereco) {
	  var tag_container = document.getElementById(elemento);
	  tag_container.style.opacity = 0.2;

	  if (elemento == 'conteudo') 
		  document.getElementById('loading').style.display = 'block';
	
	  xmlhttp.open('GET', endereco, true);
	  xmlhttp.onreadystatechange = function() {

	    if (xmlhttp.readyState == 4) {
	      retorno = xmlhttp.responseText;
	      tag_container.innerHTML=retorno;	
	      tag_container.style.opacity = 1;
	      
		  ga('send', 'pageview', {'page': endereco, 'title': titulo});
		  
		  if (elemento == 'conteudo') 
			  document.getElementById('loading').style.display = 'none';
		  
		  if (numero == -1) 
			  for (var i=2; i<=max_barras;i++) 
				  if (document.getElementById("barra" + i).innerHTML == '') {
					  numero = i;
					  break;
				  }
		  
		  if (numero != null) {
		    var elemento2 = document.getElementById("barra" + numero);
		    elemento2.innerHTML = '<a href="' + endereco + '" onclick="carrega(' + numero + ',\'' + titulo + '\',\'' + elemento + '\',\'' + endereco + '\');return false"><span class="barra">' + titulo + '</span></a><span class="barra">&nbsp;|&nbsp;</span>';
		    for (var i=numero+1; i<= max_barras; i++) document.getElementById("barra" + i).innerHTML = '';
		  }

	    }
	  };
	  xmlhttp.send(null);
}

function carrega2(numero, titulo, elemento, endereco, elemento2, endereco2) {
	  var tag_container = document.getElementById(elemento);
	  tag_container.style.opacity = 0.2;
	  
	  if (elemento == 'conteudo') 
		  document.getElementById('loading').style.display = 'block';
	
	  xmlhttp.open('GET', endereco, true);
	  xmlhttp.onreadystatechange = function() {

	    if (xmlhttp.readyState == 4) {
	      retorno = xmlhttp.responseText;
	      tag_container.innerHTML=retorno;	
	      tag_container.style.opacity = 1;
	      
		  if (elemento == 'conteudo') 
			  document.getElementById('loading').style.display = 'none';
		  
		  carrega(numero, titulo, elemento2, endereco2);
	    }
	  };
	  xmlhttp.send(null);
}

function carrega_mainmenu(lang) {
	  xmlhttp.open('GET', '/infobol/mainmenu.jsp?lang=' + lang, true);
	  xmlhttp.onreadystatechange = function() {

		if (xmlhttp.readyState == 4) {
		  retorno = xmlhttp.responseText;
		  var tag_container = document.getElementById('navegacao');
		  tag_container.innerHTML=retorno;
		  document.getElementById('cal1').innerHTML = loadCalendar(new Date(), lang, 1);
		  document.getElementById('cal2').innerHTML = loadCalendar(new Date(), lang, 0);
		  document.getElementById('cal3').innerHTML = loadCalendar(new Date(), lang, -1);
		}
	  };
	  xmlhttp.send(null);
}

function carrega_mainmenu_e_conteudo(lang) {
	  xmlhttp.open('GET', '/infobol/mainmenu.jsp?lang=' + lang, true);
	  xmlhttp.onreadystatechange = function() {

		if (xmlhttp.readyState == 4) {
		  retorno = xmlhttp.responseText;
		  var tag_container = document.getElementById('navegacao');
		  tag_container.innerHTML=retorno;
		  document.getElementById('cal1').innerHTML = loadCalendar(new Date(), lang, 0);
		  document.getElementById('cal2').innerHTML = loadCalendar(new Date(), lang, 1);
		  document.getElementById('cal3').innerHTML = loadCalendar(new Date(), lang, -1);
		  
		  for (var i=2; i<= max_barras; i++) document.getElementById("barra" + i).innerHTML = '';
			  
		  carrega('conteudo', '/infobol/lastmatch.jsp?header=no&lang=' + lang);
			  
		}
      };
		xmlhttp.send(null);
}

function preencheBarra(numero, valor, evento) {
	elemento = document.getElementById("barra" + numero);
	elemento.innerHTML = '<a href="javascript:void(0)" onclick=' + evento.replace(/&quot;/g, "'") + '><span class="barra">' + valor + '</span></a><span class="barra">&nbsp;|&nbsp;</span>';
	for (var i=valor+1; i<= 4; i++) document.getElementById("barra" + i).innerHTML = '';
}

function resizeWindow() {
    document.getElementById("navegacao").style.height = (window.innerHeight - 128) + 'px';
    document.getElementById("principal").style.height = (window.innerHeight - 128) + 'px';
}

function loadCalendar(aDay, lang, lastMonth) {
	var i = 0;
	var tmp = "";
	  
    function pad(number, length) {
   
      var str = '' + number;
      while (str.length < length) {
          str = '0' + str;
      }
   
      return str;

    }
  
    function montharr(m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11) {
      this[0] = m0;
      this[1] = m1;
      this[2] = m2;
      this[3] = m3;
      this[4] = m4;
      this[5] = m5;
      this[6] = m6;
      this[7] = m7;
      this[8] = m8;
      this[9] = m9;
      this[10] = m10;
      this[11] = m11;
    }  
  
    function monthnames(m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11) {
      this[0] = m0;
      this[1] = m1;
      this[2] = m2;
      this[3] = m3;
      this[4] = m4;
      this[5] = m5;
      this[6] = m6;
      this[7] = m7;
      this[8] = m8;
      this[9] = m9;
      this[10] = m10;
      this[11] = m11;
    } 
 
    var monthDays = new montharr(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    var monthNames = "";
	if (lang.toLowerCase() == "pt_br") {
	  monthNames = new monthnames("Janeiro", "Fevereiro", "Mar\u00e7o", "Abril", 
	                 "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", 
					 "Novembro", "Dezembro");	
	}
    if (lang.toLowerCase() == "en") {
	  monthNames = new monthnames("January", "February", "March", "April", 
	                 "May", "June", "July", "August", "September", "October", 
					 "November", "December");	
	}
    
    if (lang.toLowerCase() == "es") {
  	  monthNames = new monthnames("Enero", "Febrero", "Marzo", "Abril", 
  	                 "Mayo", "Junio", "Julio", "Agosto", "September", "Octubre", 
  					 "Noviembre", "Diciembre");	
  	}	
    
					 
    year = aDay.getFullYear();
	month = aDay.getMonth();
	day = aDay.getDate();
	today = new Date();
	
    if (lastMonth == -1) {
    	month = month - 1;		
    	day = 1;

    	if (month < 0) {
          month = 11;
    	  year = year - 1;
    	  aDay.setFullYear(year);
        }	
    	aDay.setDate(day);
        aDay.setMonth(month); 
    }
    
    if (lastMonth == -2) {
    	month = month - 2;			
    	day = 1;    	

    	if (month < 0) {
          month = 12 + month;
    	  year = year - 1;
    	  aDay.setFullYear(year);
        }	
    	aDay.setDate(day);    	
        aDay.setMonth(month);     	
    }    
    
    if (lastMonth == 1) {
    	month = month + 1;		
    	day = 1;    	

    	if (month > 11) {
          month = 0;
    	  year = year + 1;
    	  aDay.setFullYear(year);
        }	
    	aDay.setDate(day);    	
        aDay.setMonth(month);     	
    }       
	
    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
      monthDays[1] = 29;
    }

    nDays = monthDays[month];
    firstDay = aDay;
    firstDay.setDate(1);
    startDay = firstDay.getDay();
    
    
    tmp = "<table class='calendar' border='1'>";
    tmp = tmp + "<tr><th class='calendarday' colspan='5'>" + monthNames[month] + "<th class='calendarday' colspan='2'>" + year;
    
    if (lang == "en") 
    	tmp = tmp + "<tr class='calendar'><th>Sun<th>Mon<th>Tue<th>Wed<th>Thu<th>Fri<th>Sat</tr>";
    else if (lang == "es")
    	tmp = tmp + "<tr class='calendar'><th>Dom<th>Lun<th>Mar<th>Mie<th>Jue<th>Vie<th>Sab</tr>";
    else
    	tmp = tmp + "<tr class='calendar'><th>Dom<th>Seg<th>Ter<th>Qua<th>Qui<th>Sex<th>Sab</tr>";
    
    tmp = tmp + "<tr>";
    column = 0;
    for (i = 0; i < startDay; i++) {
    	tmp = tmp + "<td class='calendarday'>";
      column++;
    }
    
    for (i=1; i <= nDays; i++) {
      if ((i == today.getDate()) && (year == today.getFullYear()) && (month == today.getMonth()))
    	  tmp = tmp + "<td class='calendartoday'>";
      else
    	  tmp = tmp + "<td class='calendarday'>";
      
      if (lang == "en") 
    	  titulo = 'Matches of ' + pad(month + 1, 2) + '/' + pad(i, 2) + '/' + year;
      else if (lang == "es")
    	  titulo = 'Juego del d&iacute;a ' + pad(i, 2) + '/' + pad(month + 1, 2) + '/' + year;
      else
    	  titulo = 'Jogos do dia ' + pad(i, 2) + '/' + pad(month + 1, 2) + '/' + year;

      tmp = tmp + "<a href=\"/infobol/matches.jsp?date=" + year + pad(month + 1, 2) + pad(i, 2) + "\" onclick=\"carrega(-1, '" + titulo + "', 'conteudo', '/infobol/matches.jsp?date=" + year + pad(month + 1, 2) + pad(i, 2) +"&header=no&lang=" + lang+"');return false\">" + i + "</a>";

      column++;
      if (column == 7) {
    	  tmp = tmp + "<tr>";
        column = 0;
      }
    }
    tmp = tmp + "</table>";
    
    return tmp;
}

function changeComboCountry(elementoEstado, elementoEquipe, valorPais) {
	  xmlhttp.open('GET', "/infobol/estados.jsp?pais="+ valorPais, true);
	  xmlhttp.onreadystatechange = function() {

	    if (xmlhttp.readyState == 4) {
	      retorno = eval(xmlhttp.responseText);
	      var selectEstado = document.getElementById(elementoEstado);
	      var selectEquipe = document.getElementById(elementoEquipe);
	      
	      var i;
	      
	      if (selectEstado.options.length > 0) {
	      for(i=selectEstado.options.length-1;i>=0;i--)
	    	  selectEstado.remove(i);
	      } 
	      
	  	  for(i = 0; i < retorno.length; i+=2) {
		      var code = retorno[i];
		      var text = retorno[i+1];
		      var el = document.createElement("option");
		      el.textContent = text;
		      el.value = code;
		      if (code == "RJ") el.selected = true;
		      selectEstado.appendChild(el);
	  	  }
	  	  changeComboState(selectEstado.value, selectEquipe.name, valorPais);
	    }
	 };
	 xmlhttp.send(null);
}


function changeComboState(valorEstado, elementoEquipe, valorPais) {
	  xmlhttp.open('GET', "/infobol/equipes.jsp?pais=" + valorPais + "&estado="+ valorEstado, true);
	  xmlhttp.onreadystatechange = function() {

	    if (xmlhttp.readyState == 4) {
	      retorno = eval(xmlhttp.responseText);
	      var selectEquipe = document.getElementById(elementoEquipe);
	      
	      var i;
	      for(i=selectEquipe.options.length-1;i>=0;i--)
	    	  selectEquipe.remove(i);
	      
	  	  for(i = 0; i < retorno.length; i+=2) {
		      var code = retorno[i];
		      var text = retorno[i+1];
		      var el = document.createElement("option");
		      el.textContent = text;
		      el.value = code;
		      if (text == "Flamengo") el.selected = true;
		      selectEquipe.appendChild(el);
	  	  }
	  	  changeComboTeam(selectEquipe.value, ano.name);
	    }
	 };
	 xmlhttp.send(null);
}

function changeComboTeam(valorEquipe, elementoAno) {
    var selectAno = document.getElementById(elementoAno);
    var i;
    for(i=selectAno.options.length-1;i>=0;i--) selectAno.remove(i);
    var el = document.createElement("option");
    el.textContent = "Carregando...";
    el.value = "Carregando...";
    selectAno.appendChild(el);
    
	  xmlhttp.open('GET', "/infobol/equipesanos.jsp?cod=" + valorEquipe, true);
	  xmlhttp.onreadystatechange = function() {

	    if (xmlhttp.readyState == 4) {
	      retorno = eval(xmlhttp.responseText);
	      var selectAno = document.getElementById(elementoAno);
	      
	      var i;
	      for(i=selectAno.options.length-1;i>=0;i--)
	    	  selectAno.remove(i);
	      
	  	  for(i = 0; i < retorno.length; i+=2) {
		      var code = retorno[i];
		      var text = retorno[i+1];
		      var el = document.createElement("option");
		      el.textContent = text;
		      el.value = code;
		      selectAno.appendChild(el);
	  	  }
	    }
	 };
	 xmlhttp.send(null);
}

function recarrega(endereco, elemento, intervalo) {

	  var tag_container = document.getElementById(elemento);

	  xmlhttp.open('GET', endereco, true);
	  xmlhttp.onreadystatechange = function() {

	    if (xmlhttp.readyState == 4) {
	      retorno = xmlhttp.responseText;
	      tag_container.innerHTML=retorno;
	      setTimeout(function() {recarrega(endereco, 'conteudo', intervalo)}, intervalo);
	    }
	  };
	  xmlhttp.send(null);
};