package br.com.sgs.toolbox.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateManager {

	public DateManager() {
		super();

	}	

	public String getSysDate(String pattern) throws Exception {

		SimpleDateFormat df = new SimpleDateFormat();
		Date date = new Date(System.currentTimeMillis());
		df.applyPattern(pattern);
		return (df.format(date));		
	}


	//	retorna o dia da semana dada uma data  
	public static String getDayofWeek(String data, String lang)  
	{  
		int ano, mes, dia;

		if (data.length() != 10) return("");

		try {
			dia = Integer.parseInt(data.substring(0, 2));
			mes = Integer.parseInt(data.substring(3, 5));
			ano = Integer.parseInt(data.substring(6, 10));
		}
		catch (Exception e) {
			return("");
		}

		if ( (dia <= 0) || (dia > 31) ) return("");
		if ( (mes <= 0) || (mes > 12) ) return("");
		if (ano < 1800) return("");

		GregorianCalendar calendario = new GregorianCalendar(ano, mes - 1, dia);  
		int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);  

		if (lang.equalsIgnoreCase("en")) {
			switch (diaSemana)  
			{  

			case 1:  
				return("Sunday");  
			case 2:  
				return("Monday");  
			case 3:  
				return("Tuesday");  
			case 4:  
				return("Wednesday");  
			case 5:  
				return("Thursday");  
			case 6:  
				return("Friday");  
			case 7:  
				return("Saturday");  
			}
		}
		else if (lang.equalsIgnoreCase("es")) {
			switch (diaSemana)  
			{  
			case 1:  
				return("Domingo");  
			case 2:  
				return("Lunes");  
			case 3:  
				return("Martes");  
			case 4:  
				return("Miércoles");  
			case 5:  
				return("Jueves");  
			case 6:  
				return("Viernes");  
			case 7:  
				return("Sábado");
			}
		}
		else {
			switch (diaSemana)  
			{  

			case 1:  
				return("Domingo");  
			case 2:  
				return("Segunda");  
			case 3:  
				return("Ter&ccedil;a");  
			case 4:  
				return("Quarta");  
			case 5:  
				return("Quinta");  
			case 6:  
				return("Sexta");  
			case 7:  
				return("S&aacute;bado");  
			}


		}

		return("");	    

	}  
}
