/*
 * Created on 28/07/2005 by GRM
 *
 * 
 * 
 */
package br.com.sgs.toolbox.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;

public class DB {

	protected Connection conn = null;	
	private String dbName;

	public DB() throws Exception
	{

			try {
				conn = DataSourceFactory.getFactory().lookupDataSource("jdbc/sgs").getConnection();
				DatabaseMetaData meta = conn.getMetaData();
                this.dbName = meta.getDatabaseProductName();
				
			} catch (Exception e) {
				conn = null;
				throw new Exception(e.getMessage(),e);
			}

	}

	public Connection getConnection()	{
		return conn;
	}	
	
	public String getDBName() {
		if (this.dbName.toLowerCase().contains("firebird")) return "firebird";
		return "mysql";
	}

}
