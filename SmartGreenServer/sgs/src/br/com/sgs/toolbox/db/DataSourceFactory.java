package br.com.sgs.toolbox.db;

import java.util.Collections;
import java.util.HashMap;
//import java.util.Hashtable;  
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class DataSourceFactory {
	
	private Map<String, DataSource> datasource;
	
	private InitialContext envContext;
//	private InitialContext ctx;	
	private Context ctx;	
	private static DataSourceFactory instance;	
	
	private DataSourceFactory() throws Exception
	{
/*		Hashtable<String, String> t = new Hashtable<String, String>();

        t.put ( Context.INITIAL_CONTEXT_FACTORY ,"org.jnp.interfaces.NamingContextFactory");
    	t.put ( Context.PROVIDER_URL , "localhost:1099") ;
    	t.put ( Context.URL_PKG_PREFIXES , "org.jboss.naming:org.jnp.interfaces") ;
		ctx = new InitialContext(t); */

		
	
		envContext = new InitialContext();
		ctx  = (Context)envContext.lookup("java:/comp/env"); 		

		
		datasource = Collections.synchronizedMap(new HashMap<String, DataSource>());		
	}
	
	public static DataSourceFactory getFactory() throws Exception
	{
		if (instance == null)
		{
			instance = new DataSourceFactory();
		}
		return instance;
	}	
	
	/**
	 * Busca por uma referencia do datasource no cache, se nao existir nenhuma referencia pesquisa na JNDI
	 * e armazena no cache.
	 * 
	 * @param jndiName String JNDI
	 * @return DataSource
	 * @throws NamingException
	 */
	public DataSource lookupDataSource(String jndiName) throws NamingException
	{
		// Procura pela referencia Remote Home no cache
		DataSource ds = (DataSource) datasource.get(jndiName);

		if (ds == null)
		{
			ds = (DataSource) ctx.lookup(jndiName);
			
			//Alimenta o cache de referencia ao Objeto EJBHome
			datasource.put(jndiName,ds);
		}
		return ds;
	}
	
}
