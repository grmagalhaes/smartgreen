package br.com.sgs.bo;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.http.HttpServletRequest;

import br.com.sgs.toolbox.db.DB;
import br.com.sgs.toolbox.json.JSONHTTP;
import br.com.sgs.toolbox.json.JSONObject;
import br.com.sgs.toolbox.json.JSONTokener;

/**
 * Classe utilizada para manipular Cartões :: tanto no CRUD quando nas traasações
 * 
 * @author Gerson Rodrigues
 * @since 1.0
 * 
 */


public class Merchant {


	private String body;	
	private JSONObject jsonobject;

	private String id;
	private String name;
	private String address;
	private String city;
	private String state;
	private String cnpj;
	private String serialNumber;
	private String stoneCode;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public JSONObject getJsonobject() {
		return jsonobject;
	}

	public void setJsonobject(JSONObject jsonobject) {
		this.jsonobject = jsonobject;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getStoneCode() {
		return stoneCode;
	}

	public void setStoneCode(String stoneCode) {
		this.stoneCode = stoneCode;
	}   

	public void createJSONParser(HttpServletRequest request) throws Exception {

		JSONHTTP jsonhttp = new JSONHTTP();
		this.body = jsonhttp.getJSON(request);

		JSONTokener jsontokener = new JSONTokener(this.body);
		jsonobject = new JSONObject(jsontokener);
		
		this.setId(jsonobject.getJSONObject("merchant").getString("id"));
		this.setName(jsonobject.getJSONObject("merchant").getString("name"));
		this.setAddress(jsonobject.getJSONObject("merchant").getString("address"));
		this.setCity(jsonobject.getJSONObject("merchant").getString("city"));			
		this.setState(jsonobject.getJSONObject("merchant").getString("state"));	  
		this.setCnpj(jsonobject.getJSONObject("merchant").getString("cnpj"));	   
		this.setSerialNumber(jsonobject.getJSONObject("merchant").getString("serial_number"));	
		this.setStoneCode(jsonobject.getJSONObject("merchant").getString("stone_code"));
	}

	// Getters e Setters 


	public boolean create() throws Exception {
		Connection conn = new DB().getConnection();
		boolean ret = false;

		// merchant_
		StringBuffer sql = new StringBuffer();		   

		sql.append("insert into merchant_ (id, name, address, city, state_, cnpj) ");
		sql.append("values ( ?, ?, ?, ?, ?, ?)");
		
		PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getId());		   
		pstmt.setString(2, this.getName());		   
		pstmt.setString(3, this.getAddress());		   
		pstmt.setString(4, this.getCity());		   
		pstmt.setString(5, this.getState());		   
		pstmt.setString(6, this.getCnpj());		   

		pstmt.executeUpdate();
		pstmt.close();
		
		// equipment_
		sql = new StringBuffer();		   
		
		sql.append("insert into equipment_ (merchant_id, stone_code, serial) ");
		sql.append("values ( ?, ?, ?)");
		
		pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getId());		   
		pstmt.setString(2, this.getStoneCode());		   
		pstmt.setString(3, this.getSerialNumber());		   

		pstmt.executeUpdate();
		pstmt.close();
		conn.close();		

		ret = true;

		return ret;

	}	

	public boolean delete() throws Exception {
		Connection conn = new DB().getConnection();
		boolean ret = false;

		StringBuffer sql = new StringBuffer();
		
		sql.append("delete from equipment_ where merchant_id = ?");

		PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getId());		   

		pstmt.executeUpdate();
		pstmt.close();		
		
		sql = new StringBuffer();
		sql.append("delete from merchant_ where id = ?");

		pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getId());		   

		pstmt.executeUpdate();
		pstmt.close();
		conn.close();

		ret = true;

		return ret;

	}	

}
