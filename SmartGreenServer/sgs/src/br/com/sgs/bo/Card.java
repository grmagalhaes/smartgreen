package br.com.sgs.bo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.http.HttpServletRequest;

import br.com.sgs.toolbox.db.DB;
import br.com.sgs.toolbox.json.JSONHTTP;
import br.com.sgs.toolbox.json.JSONObject;
import br.com.sgs.toolbox.json.JSONTokener;

/**
 * Classe utilizada para manipular Cartões :: tanto no CRUD quando nas traasações
 * 
 * @author Gerson Rodrigues
 * @since 1.0
 * 
 */


public class Card {
	
   private String body;	
   private JSONObject jsonobject;
   
   private String cardNumber;
   private String cardHolderName;
   private String cardBrand;
   private String expirationDate;
   private String type;
   private String accept;
   private boolean hasPassword;
   private String currency;
   private String password;
   private int passwordSize;
   private boolean acceptedByStone;
   
   private double balance;
   private double limit;
   
   private boolean lost;
   private boolean robbed;
   private boolean blocked;
   
   public void createJSONParser(HttpServletRequest request) throws Exception {

		JSONHTTP jsonhttp = new JSONHTTP();
		this.body = jsonhttp.getJSON(request);
				
		JSONTokener jsontokener = new JSONTokener(this.body);
		jsonobject = new JSONObject(jsontokener);

		this.setCardNumber(jsonobject.getJSONObject("card").getString("number"));
		this.setCardHolderName(jsonobject.getJSONObject("card").getString("card_holder_name"));
		this.setCardBrand(jsonobject.getJSONObject("card").getString("brand"));	
		this.setExpirationDate(jsonobject.getJSONObject("card").getString("expiration_date"));	  
		this.setType(jsonobject.getJSONObject("card").getString("type"));	   
		this.setAccept(jsonobject.getJSONObject("card").getString("accept"));	
		this.setHasPassword(jsonobject.getJSONObject("card").getString("has_password").equalsIgnoreCase("True"));	   
		this.setCurrency(jsonobject.getJSONObject("card").getString("currency"));	
		this.setPassword(jsonobject.getJSONObject("card").getString("password"));	
		this.setPasswordSize(jsonobject.getJSONObject("card").getInt("password_size"));		
		this.setBalance(jsonobject.getJSONObject("card").getDouble("balance"));		
		this.setLimit(jsonobject.getJSONObject("card").getDouble("limit"));		
		this.setLost(jsonobject.getJSONObject("card").getString("lost").equalsIgnoreCase("True"));		
		this.setRobbed(jsonobject.getJSONObject("card").getString("robbed").equalsIgnoreCase("True"));		
		this.setBlocked(jsonobject.getJSONObject("card").getString("blocked").equalsIgnoreCase("True"));		
	}	
   
  // Getters e Setters 
   
   public String getCardNumber() {
	   return this.cardNumber;
   }   
   
   public void setCardNumber(String cardNumber) {
	   this.cardNumber = cardNumber;
	   
   }
   
   public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAccept() {
		return accept;
	}

	public void setAccept(String accept) {
		this.accept = accept;
	}

	public boolean getHasPassword() {
		return hasPassword;
	}

	public void setHasPassword(boolean hasPassword) {
		this.hasPassword = hasPassword;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPasswordSize() {
		return passwordSize;
	}

	public void setPasswordSize(int passwordSize) {
		this.passwordSize = passwordSize;
	}

	public boolean getAcceptedByStone() {
		return acceptedByStone;
	}

	public void setAcceptedByStone(boolean acceptedByStone) {
		this.acceptedByStone = acceptedByStone;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getLimit() {
		return limit;
	}

	public void setLimit(double limit) {
		this.limit = limit;
	}

	public boolean isLost() {
		return lost;
	}

	public void setLost(boolean lost) {
		this.lost = lost;
	}

	public boolean isRobbed() {
		return robbed;
	}

	public void setRobbed(boolean robbed) {
		this.robbed = robbed;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	} 
	
	public boolean create() throws Exception {
		   Connection conn = new DB().getConnection();
		   boolean ret = false;
		   
		   StringBuffer sql = new StringBuffer();		   
		   
	 	   sql.append("insert into card_ (card_number, card_holder_name, expiration_date, card_brand, card_type, card_accept, has_password, ");
	 	   sql.append("password_, password_size, credit_limit, balance, robbed, lost, blocked) ");
	 	   sql.append("values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	 	   
		   PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		   pstmt.setString(1, this.getCardNumber());		   
		   pstmt.setString(2, this.getCardHolderName());		   
		   pstmt.setString(3, this.getExpirationDate());		   
		   pstmt.setString(4, this.getCardBrand());		   
		   pstmt.setString(5, this.getType());		   
		   pstmt.setString(6, this.getAccept());		   
		   pstmt.setString(7, (this.getHasPassword() ? "S" : "N"));		   
		   pstmt.setString(8, this.getPassword());		   
		   pstmt.setInt(9, this.getPasswordSize());		   
		   pstmt.setDouble(10, this.getLimit());		   
		   pstmt.setDouble(11, this.getBalance());		   
		   pstmt.setString(12, (this.isRobbed() ? "S" : "N"));		   
		   pstmt.setString(13, (this.isLost() ? "S" : "N"));		   
		   pstmt.setString(14, (this.isBlocked() ? "S" : "N"));		   
		   
		   pstmt.executeUpdate();
		   pstmt.close();
		   conn.close();
		   
		   ret = true;
		   
		   return ret;
		
	}	
	
	public boolean read() throws Exception {
		   Connection conn = new DB().getConnection();
		   boolean ret = false;
		   
		   // zera valores antes da carga do cartão da operadora
		   this.setCardHolderName("");
		   this.setExpirationDate("");		
		   this.setCardBrand("");	
		   this.setType("");
		   this.setAccept("");			
		   this.setHasPassword(true);	
		   this.setPassword("");	
		   this.setPasswordSize(0);	
		   this.setLimit(0.0);	
		   this.setBalance(0.0);			   
		   this.setRobbed(false);		
		   this.setLost(false);		
		   this.setBlocked(false);			   
		   
		   StringBuffer sql = new StringBuffer();		   
		   
	 	   sql.append("select * from card_ where card_number = ?");
	 	   
		   PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		   pstmt.setString(1, this.getCardNumber());		   
		   
		   ResultSet rs = pstmt.executeQuery();
		   
		   while (rs.next()) {
			   this.setCardHolderName(rs.getString("card_holder_name"));
			   this.setExpirationDate(rs.getString("expiration_date"));		
			   this.setCardBrand(rs.getString("card_brand"));	
			   this.setType(rs.getString("card_type"));
			   this.setAccept(rs.getString("card_accept"));			
			   this.setHasPassword(rs.getString("has_password").equalsIgnoreCase("S"));	
			   this.setPassword(rs.getString("password_"));	
			   this.setPasswordSize(rs.getInt("password_size"));	
			   this.setLimit(rs.getFloat("credit_limit"));	
			   this.setBalance(rs.getFloat("balance"));			   
			   this.setRobbed(rs.getString("robbed").equalsIgnoreCase("S"));		
			   this.setLost(rs.getString("lost").equalsIgnoreCase("S"));		
			   this.setBlocked(rs.getString("blocked").equalsIgnoreCase("S"));
			   ret = true;
		   }

		   rs.close();
		   pstmt.close();
		   conn.close();
		   
		   return ret;
		
	}
	
	public boolean delete() throws Exception {
		   Connection conn = new DB().getConnection();
		   boolean ret = false;
		   
		   StringBuffer sql = new StringBuffer();		   
		   
	 	   sql.append("delete from card_ where card_number = ?");
	 	   
		   PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		   pstmt.setString(1, this.getCardNumber());		   
		   
		   pstmt.executeUpdate();
		   pstmt.close();
		   conn.close();
		   
		   ret = true;
		   
		   return ret;
		
	}	
		
	
	public boolean updateBalance(double amount) throws Exception {
		   Connection conn = new DB().getConnection();
		   boolean ret = false;
		   
		   StringBuffer sql = new StringBuffer();		   
		   
	 	   sql.append("update card_ set balance = ? where card_number = ?");
	 	   
		   PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		   pstmt.setDouble(1, this.getBalance() + amount);		
		   pstmt.setString(2, this.getCardNumber());			   
		   
		   pstmt.executeUpdate();
		   
		   pstmt.close();
		   conn.close();
		   
		   return ret;
		
	}
	

}
