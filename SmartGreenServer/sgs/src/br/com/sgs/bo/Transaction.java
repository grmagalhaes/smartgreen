package br.com.sgs.bo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import br.com.sgs.toolbox.crypto.SHA256;
import br.com.sgs.toolbox.db.DB;
import br.com.sgs.toolbox.json.JSONArray;
import br.com.sgs.toolbox.json.JSONHTTP;
import br.com.sgs.toolbox.json.JSONObject;
import br.com.sgs.toolbox.json.JSONTokener;

/**
 * Classe responsável por gerenciar uma transação do cartão
 * 
 * @author Gerson Rodrigues
 * @author Origine Tecnologia
 * @since 1.0
 * 
 */

public class Transaction {

	private String body;	
	private JSONObject jsonobject;

	private String currency;
	private double amount;
	private String type;
	private int numberInstallments;
	private String transactionTS;
	private String merchant; 
	private String stoneCode; 
	private String serial;

	private String authorization;
	private String status;
	private String messageCode;

	private Card card;
	private Card cardBase;

	public Transaction() {
		this.card = new Card();
		this.cardBase = new Card();
	}

	/**
	 * Mensagem de Retorno de uma transação. 
	 * 
	 * @author Gerson Rodrigues
	 * @since 1.0
	 * 
	 */

	public String getMessage(String code) {
		if (code.equals("0000")) return "Aprovado";
		if (code.equals("0001")) return "Aprovado após verificar a identidade";
		if (code.equals("1009")) return "Estabelecimento inválido";
		if (code.equals("1010")) return "Valor inválido. Valor aceito deve ser >= 0.10";
		if (code.equals("1011")) return "Cartão inválido";
		if (code.equals("1016")) return "Saldo insuficiente";
		if (code.equals("1017")) return "Senha inválida";
		if (code.equals("1018")) return "Erro no tamanho da senha : deve ter entre 4 e 6 dígitos";
		if (code.equals("1025")) return "Cartão bloqueado";
		if (code.equals("2000")) return "Transação Negada";
		if (code.equals("2001")) return "Cartão vencido";
		if (code.equals("2008")) return "Cartão perdido";
		if (code.equals("2009")) return "Cartão roubado";
		if (code.equals("9107")) return "Emissor fora de operação";
		if (code.equals("9108")) return "Não foi possível enviar a transação para o destinatário";
		if (code.equals("9107")) return "Bandeira não aceita";
		if (code.equals("9109")) return "Erro no sistema"; 
		if (code.equals("9109")) return "Equipamento não habilitado"; 
		if (code.equals("9109")) return "Erro não especificado"; 	   
		return "Erro não especificado"; 

	}

	/**
	 * Parser do JSON com a transação
	 * 
	 * @author Gerson Rodrigues
	 * @author Origine Tecnologia
	 * @throws Exception 
	 * @since 1.0
	 * 
	 */  

	public void createJSONParser(HttpServletRequest request) throws Exception {
		    JSONHTTP jsonhttp = new JSONHTTP();
		    this.body = jsonhttp.getJSON(request);
		    
			JSONTokener jsontokener = new JSONTokener(this.body);
			jsonobject = new JSONObject(jsontokener);

			this.currency = jsonobject.getJSONObject("transaction").getString("currency");
			try {
			  this.amount = jsonobject.getJSONObject("transaction").getDouble("amount");
			}
			catch(Exception e) {
			  this.amount = jsonobject.getJSONObject("transaction").getInt("amount");
			}
			this.type = jsonobject.getJSONObject("transaction").getString("type");
			this.numberInstallments = jsonobject.getJSONObject("transaction").getInt("number_installments");
			this.transactionTS = jsonobject.getJSONObject("transaction").getString("transaction_ts");
			this.merchant = jsonobject.getJSONObject("transaction").getString("merchant"); 
			this.stoneCode = jsonobject.getJSONObject("transaction").getString("stonecode"); 
			this.serial = jsonobject.getJSONObject("transaction").getString("serial");

			this.getCard().setCardNumber(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("number"));
			this.getCard().setCardHolderName(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("card_holder_name"));
			this.getCard().setCardBrand(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("brand"));	
			this.getCard().setExpirationDate(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("expiration_date"));	  
			this.getCard().setType(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("type"));	   
			this.getCard().setAccept(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("accept"));	
			this.getCard().setHasPassword(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("has_password").equalsIgnoreCase("True"));	   
			this.getCard().setCurrency(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("currency"));	
			this.getCard().setPassword(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("password"));	
			this.getCard().setPasswordSize(jsonobject.getJSONObject("transaction").getJSONObject("card").getInt("password_size"));		
			this.getCard().setAcceptedByStone(jsonobject.getJSONObject("transaction").getJSONObject("card").getString("accepted_by_stone").equalsIgnoreCase("True"));
}


	/**
	 * Validadores na ordem
	 * 
	 * @author Gerson Rodrigues
	 * @throws Exception 
	 * @since 1.0
	 * 
	 */ 

	// Validador global
	public boolean validate() throws Exception {

		this.messageCode = "9999"; // Erro não especificado
		this.status = "4"; // Cancelado

		this.getCardBase().setCardNumber(this.getCard().getCardNumber());;
		this.getCardBase().read();	   

		if (!validateCardAccepted()) {
			this.messageCode = "9107";
			return false;
		}

		if (!validateCard()) {
			this.messageCode = "1011";
			return false;		   
		}

		if (!validateRobbed()) {
			this.messageCode = "2009";
			return false;		   
		}

		if (!validateLost()) {
			this.messageCode = "2008";
			return false;		   
		}
		
		if (!validateBlocked()) {
			this.messageCode = "1025";
			return false;		   
		}		

		if (!validateExpirationDate()) {
			this.messageCode = "2001";
			return false;		   
		}

		if (validateHasPassword()) {
			if (!validatePassword()) {
				this.messageCode = "1017";
				return false;		   
			}
		}

		if (validateHasPassword()) {	   
			if (!validatePasswordSize()) {
				this.messageCode = "1018";
				return false;		   
			}
		}

		if (!validateMerchant()) {
			this.messageCode = "1009";
			return false;		   
		}

		if (!validateValue()) {
			this.messageCode = "1010";
			return false;		   
		}

		if (!validateBalance()) {
			this.messageCode = "1016";
			return false;			   
		}

		// se não tiver senha, aprovar com verificacao de documento
		if (!validateHasPassword()) {
			this.messageCode = "0001";
		} else {
			this.messageCode = "0000";		   
		}

		this.status = "3";
		return true;
	}


	// Cartão aceito pela Stone?
	public boolean validateCardAccepted() {
		return this.getCard().getAcceptedByStone();
	}

	// Cartao Inválido
	public boolean validateCard() {
		if (this.getCard().getCardNumber().equals(this.getCardBase().getCardNumber()) &&
				this.getCard().getCardHolderName().equalsIgnoreCase(this.getCardBase().getCardHolderName()) &&
				this.getCard().getExpirationDate().equals(this.getCardBase().getExpirationDate()))
			return true;
		else return false;
	}   

	// Cartao Roubado
	public boolean validateRobbed() {
		if (!this.getCardBase().isRobbed())
			return true;
		else
			return false;
	}  

	// Cartao Perdido
	public boolean validateLost() {
		if (!this.getCardBase().isLost())
			return true;
		else
			return false;
	}  
	
	// Cartao Bloqueado
	public boolean validateBlocked() {
		if (!this.getCardBase().isBlocked())
			return true;
		else
			return false;
	}  	

	// Cartao Expirado
	public boolean validateExpirationDate() {
		Date date = new Date(); // data de hoje
		Calendar cal = Calendar.getInstance();
		
		String expirationDate = "20" + this.getCardBase().getExpirationDate().substring(3, 5) + this.getCardBase().getExpirationDate().substring(0, 2);

		cal.setTime(date);
		String now = String.format("%04d", cal.get(Calendar.YEAR)) + String.format("%02d", cal.get(Calendar.MONTH) + 1);
		
		return (expirationDate.compareTo(now) >= 0);
	}  

	// Senha Inválida
	public boolean validatePassword() {
		if (this.getCard().getPassword().equals(this.getCardBase().getPassword()))
			return true;
		else return false;
	}    

	// Senha Pequena
	public boolean validatePasswordSize() {
		return ((this.getCard().getPasswordSize() >= 4) && (this.getCard().getPasswordSize() <= 6));
	}  

	// Estabelecimento válido
	public boolean validateMerchant() throws Exception {
		Connection conn = new DB().getConnection();
		boolean ret = false;

		StringBuffer sql = new StringBuffer();		   

		sql.append("select * from merchant_ where id = ?");

		PreparedStatement pstmt = conn.prepareStatement(sql.toString());	   
		pstmt.setString(1, this.getMerchant());		   

		ResultSet rs = pstmt.executeQuery();
		
		while (rs.next()) {
			ret = true;
		}

		rs.close();
		pstmt.close();
		conn.close();

		return ret;
	}    

	// Valor < 0.10
	public boolean validateValue() {
		return (this.getAmount() >= 0.10);
	}   

	// Valida se tem saldo
	public boolean validateBalance() {
		return (this.getCardBase().getLimit() >= (this.getAmount() + this.getCardBase().getBalance()));
	}    

	// Tem senha ? Caso negativa aprovar mediante verificacao de documento
	public boolean validateHasPassword() {
		return this.getCardBase().getHasPassword();
	}   

	// cria uma nova transacao
	public void create() throws Exception {
		Connection conn = new DB().getConnection();
		StringBuffer sql = new StringBuffer();
		SHA256 sha256 = new SHA256();


		boolean ret = validate();

		this.setAuthorization(sha256.getHashCode(this.getCard().getCardNumber() + System.currentTimeMillis()).substring(0, 16));

		sql.append("insert into transaction_ (authorization, card_number, amount, currency, transaction_type, ");
		sql.append("number_installments, merchant_id, status, response, password_, password_size) ");
		sql.append("values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getAuthorization()); 
		pstmt.setString(2, this.getCard().getCardNumber());
		pstmt.setDouble(3, this.getAmount());
		pstmt.setString(4, this.getCurrency());
		pstmt.setString(5, this.getType());
		pstmt.setInt(6,  this.getNumberInstallments());
		pstmt.setString(7, this.merchant);
		pstmt.setString(8, this.getStatus());
		pstmt.setString(9, this.getMessageCode());
		pstmt.setString(10, this.getCard().getPassword());
		pstmt.setInt(11, this.getCard().getPasswordSize());

		pstmt.executeUpdate();
		
		if (ret) 		// Transação aprovada! Saldo atualizado !
			this.getCardBase().updateBalance(this.getAmount());

		pstmt.close();
		conn.close();

	} 
	
	public void startJSONParser(HttpServletRequest request) throws Exception {
		
	    JSONHTTP jsonhttp = new JSONHTTP();
	    this.body = jsonhttp.getJSON(request);
	    
		JSONTokener jsontokener = new JSONTokener(this.body);
		jsonobject = new JSONObject(jsontokener);

		this.merchant = jsonobject.getJSONObject("transaction").getString("merchant"); 
		this.stoneCode = jsonobject.getJSONObject("transaction").getString("stonecode"); 
		this.serial = jsonobject.getJSONObject("transaction").getString("serial");

	}
	

// verifica se o equipamento está habilitado para fazer uma transação : serial + stone_code devem ser válidos para o estabelecimento	
	public boolean start() throws Exception {
		Connection conn = new DB().getConnection();
		boolean ret = false;

		StringBuffer sql = new StringBuffer();		   

		sql.append("select * from equipment_ where serial = ? and stone_code = ? and merchant_id = ?");

		PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getSerial());	
		pstmt.setString(2, this.getStoneCode());	   
		pstmt.setString(3, this.getMerchant());		   

		ResultSet rs = pstmt.executeQuery();
		
		while (rs.next()) {
			ret = true;
		}

		rs.close();
		pstmt.close();
		conn.close();

		return ret;

	}
	
	public void listJSONParser(HttpServletRequest request) throws Exception {
		
	    JSONHTTP jsonhttp = new JSONHTTP();
	    this.body = jsonhttp.getJSON(request);
	    
		JSONTokener jsontokener = new JSONTokener(this.body);
		jsonobject = new JSONObject(jsontokener);

		this.merchant = jsonobject.getString("id"); 

	}	
	
	public String list() throws Exception {
		Connection conn = new DB().getConnection();
		StringBuffer sql = new StringBuffer();

		sql.append("select * from  transaction_ where merchant_id = ?");
		
		PreparedStatement pstmt = conn.prepareStatement(sql.toString());
		pstmt.setString(1, this.getMerchant()); 
		
		ResultSet rs = pstmt.executeQuery();
		
		JSONArray jsonArray = new JSONArray();
		int i = 0;
		while (rs.next()) {

			JSONObject jsonItem = new JSONObject();			
			jsonItem.put("authorization", rs.getString("authorization"));
			jsonItem.put("card_number", rs.getString("card_number"));
			jsonItem.put("amount", rs.getDouble("amount"));
			jsonItem.put("currency", rs.getString("currency"));
			jsonItem.put("transaction_type", rs.getString("transaction_type"));			
			jsonItem.put("number_installments", rs.getInt("number_installments"));
			jsonItem.put("status", rs.getString("status"));			
			jsonItem.put("response", rs.getString("response"));			
			jsonItem.put("transaction_ts", rs.getString("transaction_ts"));
			
			jsonArray.put(i, jsonItem);
			i++;
		}

		rs.close();
		pstmt.close();
		conn.close();
		
		
		return jsonArray.toString();

	} 



	/**
	 * Getters e Setters
	 * 
	 * @author Gerson Rodrigues
	 * @since 1.0
	 * 
	 */ 

	public Card getCard() {
		return this.card;
	};   

	public Card getCardBase() {
		return this.cardBase;
	};   

	public String getCurrency() {
		return this.currency;
	}

	public double getAmount() {
		return this.amount;
	};  

	public String getType() {
		return this.type;
	};

	public int getNumberInstallments() {
		return this.numberInstallments;
	};

	public String getTransactionTS() {
		return this.transactionTS;
	};

	public String getMerchant() {
		return this.merchant;
	};

	public String getStoneCode() {
		return this.stoneCode;
	};

	public String getSerial() {
		return this.serial;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public JSONObject getJsonobject() {
		return jsonobject;
	}

	public void setJsonobject(JSONObject jsonobject) {
		this.jsonobject = jsonobject;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setNumberInstallments(int numberInstallments) {
		this.numberInstallments = numberInstallments;
	}

	public void setTransactionTS(String transactionTS) {
		this.transactionTS = transactionTS;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public void setStoneCode(String stoneCode) {
		this.stoneCode = stoneCode;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public void setCardBase(Card cardBase) {
		this.cardBase = cardBase;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

}
