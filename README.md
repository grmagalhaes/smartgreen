# smartgreen 
![smartgreen logo](/Images/logo2.png)

## Simulador de Operações de Cartão de Crédito/Débito ##

### Desafio para a Stone Pagamentos
### Projeto aprovado

###Transações Financeiras###

O projeto consiste na implementação de duas aplicações (cliente-servidor) que simulem o 
processo de uma transação financeira.

**O cliente**

Deve ser uma aplicação simples utilizando a tecnologia que preferir. 
O layout não será avaliado, mas a facilidade de uso da tela será. Deve haver duas telas principais:

* Tela de transação: input dos dados da transação e envio da transação para o servidor.
* Tela de consulta das transações efetuadas: lista das transações efetuadas.

**Catálogo de cartões virtuais**

Criar um catálogo de cartões virtuais (o número que você achar razoável para testar diferentes cenários), 
que estarão disponíveis quando o portador for fazer uma compra. 
Esses cartões deverão ter propriedades a mais do que o cartão "básico" descrito abaixo. 
O limite de crédito de cada cartão deve ser considerado.

* A senha de cada cartão do catálogo deve estar criptografada de algum jeito
* Com esse catálogo, a verificação da senha do cartão deve ser feita apenas pelo servidor

**O servidor**

O servidor irá simular justamente o que a Stone é, uma adquirente.
Sinta-se livre para utilizar a tecnologia que quiser, desde que cumpra os requisitos abaixo. 
O servidor deve esperar por uma transação. 
Assim que o cliente enviar uma requisição, o servidor deve usar parâmetros de validação dos 
dados da transação e parâmetros extras (você pode usar outros parâmetros, mas documente em algum canto!) 
para retornar sucesso ou erro na transação.
￼
No mundo real, uma adquirente se comunica com o banco (emissor do cartão de crédito/débito) 
e com a bandeira (Visa, MasterCard, American Express, etc). 
O servidor não precisará se preocupar com isso, apenas irá simular uma transação financeira de acordo 
com alguns parâmetros.
usar parâmetros de validação dos dados da transação e 
parâmetros extras (você pode usar outros parâmetros, mas documente em algum canto!) 
para retornar sucesso ou erro na transação.

**Códigos de retorno**

* Aprovado - Transação aprovada
* Transação negada - Transação negada
* Saldo insuficiente - Portador do cartão não possui saldo
* Valor inválido - Mínimo de 10 centavos
* Cartão bloqueado - Quando o cartão está bloqueado
* Erro no tamanho da senha - Senha deve ter entre 4 e 6 dítigos
* Senha inválida - A senha enviada é inválida

￼Você pode (e deve!) adicionar novos códigos de retorno. 

**Cadastro dos estabelecimentos comerciais**

Deverá ser capaz de cadastrar estabelecimentos comerciais que possam passar transações no seu servidor.

**Comunicação entre cliente e servidor** 

A comunicação pode acontecer em JSON ou XML.

**O que será avaliado?**

1. Desenho e arquitetura da solução
2. Padrões de projeto utilizados
3. Organização, documentação e legibilidade do código

4. Uso de biblioteca de terceiros 5. Criatividade
6. Testes de unidade e mocking
7. Qualidade do acesso a dados

Instruções
Ao finalizar, você pode nos mandar um link do repositório no GitHub/BitBucket ou um arquivo ZIP 
com todo o código desenvolvido. É muito importante que tenha as instruções de execução.

